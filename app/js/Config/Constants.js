/**
 * Created by Maikel Rivero Dorta mriverodorta@gmail.com on 7/08/14.
 */
'use strict';
angular.module('AngStarter')

.constant('JQ_CONFIG', {
    easyPieChart:   [   'libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
    sparkline:      [   'libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js'],
    plot:           [   'libs/jquery/flot/jquery.flot.js',
                        'libs/jquery/flot/jquery.flot.pie.js',
                        'libs/jquery/flot/jquery.flot.resize.js',
                        'libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                        'libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js',
                        'libs/jquery/flot-spline/js/jquery.flot.spline.min.js'],
    moment:         [   'libs/jquery/moment/moment.js'],
    screenfull:     [   'libs/jquery/screenfull/dist/screenfull.min.js'],
    slimScroll:     [   'libs/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       [   'libs/jquery/html5sortable/jquery.sortable.js'],
    nestable:       [   'libs/jquery/nestable/jquery.nestable.js',
                        'libs/jquery/nestable/jquery.nestable.css'],
    filestyle:      [   'libs/jquery/bootstrap-filestyle/src/bootstrap-filestyle.js'],
    slider:         [   'libs/jquery/bootstrap-slider/bootstrap-slider.js',
                        'libs/jquery/bootstrap-slider/bootstrap-slider.css'],
    chosen:         [   'libs/jquery/chosen/chosen.jquery.min.js',
                        'libs/jquery/chosen/bootstrap-chosen.css'],
    TouchSpin:      [   'libs/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                        'libs/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
    wysiwyg:        [   'libs/jquery/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                        'libs/jquery/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
    dataTable:      [   'libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                        'libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                        'libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    vectorMap:      [   'libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                        'libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                        'libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                        'libs/jquery/bower-jvectormap/jquery-jvectormap.css'],
    footable:       [   'libs/jquery/footable/v3/js/footable.min.js',
                        'libs/jquery/footable/v3/css/footable.bootstrap.min.css'],
    fullcalendar:   [   'libs/jquery/moment/moment.js',
                        'libs/jquery/fullcalendar/dist/fullcalendar.min.js',
                        'libs/jquery/fullcalendar/dist/fullcalendar.css',
                        'libs/jquery/fullcalendar/dist/fullcalendar.theme.css'],
    daterangepicker:[   'libs/jquery/moment/moment.js',
                        'libs/jquery/bootstrap-daterangepicker/daterangepicker.js',
                        'libs/jquery/bootstrap-daterangepicker/daterangepicker-bs3.css'],
    tagsinput:      [   'libs/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                        'libs/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'],
    select2:        [   'assets/js/plugin/select2/select2.min.js',
                        'assets/js/plugin/select2/select2.min.css']

  }
)
.constant('MODULE_CONFIG', [
    {
        name: 'ui.select',
        files: [
            'bower_components/angular-ui-select/dist/select.min.js',
            'bower_components/angular-ui-select/dist/select.css'
        ]
    },
    {
        name: 'ui.select2',
        files: [
            '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',
            '//rawgithub.com/angular-ui/ui-select2/master/src/select2.js'
        ]
    },
    {
        name: 'rt.select2',
        files: [
            'bower_components/angular-select2/dist/angular-select2.min.js'
        ]
    },
    {
        name: 'cgBusy',
        files: ['bower_components/angular-busy/dist/angular-busy.js', 'bower_components/angular-busy/dist/angular-busy.css']
    },
    // {
    //     name: 'summernote',
    //     files: ['bower_components/angular-summernote/dist/angular-summernote.min.js', 'bower_components/summernote/dist/summernote.css', 'bower_components/summernote/dist/summernote.min.js']
    // },
    {
        name: 'vcRecaptcha',
        files: ['bower_components/angular-recaptcha/release/angular-recaptcha.js']
    },
    {
        name: 'oitozero.ngSweetAlert',
        files: ['bower_components/sweetalert/dist/sweetalert.min.js', 'bower_components/ngSweetAlert/SweetAlert.min.js', 'bower_components/sweetalert/dist/sweetalert.css']
    }
    // {
    //     name: 'ngAnimate',
    //     files: [
    //         'bower_components/angular-animate/angular-animate.js'
    //     ]
    // },
    // {
    //     name: 'ngAria',
    //     files: [
    //         'bower_components/angular-aria/angular-aria.js'
    //     ]
    // },
    // {
    //     name: 'ngMaterial',
    //     files: [
    //         'bower_components/angular-material/angular-material.min.js',
    //         'bower_components/angular-material/angular-material.min.css'
    //     ]
    // },
    // {
    //     name: 'ngMaterialDatePicker',
    //     files: [
    //         'bower_components/ng-material-datetimepicker/dist/angular-material-datetimepicker.min.js',
    //         'bower_components/ng-material-datetimepicker/dist/material-datetimepicker.min.css'
    //     ]
    // }
  ]
)

    .constant('Config', {
        //api: 'http://localhost/eproc/public', //default server Laravel
       //api: 'http://10.10.8.6', //default server .Net
       // api: 'http://192.168.9.110/eproc/public', //Bari server
       // api: 'http://192.168.9.122',
       api:'http://192.168.43.79:8090/api/sertifikasi',
        //api:'http://localhost:8090/api/sertifikasi',ini yg asli
       saltKey: 'eProcSomeRandomKey098765',
       captchaGKey: '6LcFWEQUAAAAALVm_v67yawUrChvO-ediHIMQbgu', //default server
       // captchaGKey: '' //localhost
    //    timezone: getTimeZone()
   })


   // oclazyload config
   .config(['$ocLazyLoadProvider', 'MODULE_CONFIG', function($ocLazyLoadProvider, MODULE_CONFIG) {
       // We configure ocLazyLoad to use the lib script.js as the async loader
       $ocLazyLoadProvider.config({
           debug:  true,
           events: true,
           modules: MODULE_CONFIG
       });
   }]);
