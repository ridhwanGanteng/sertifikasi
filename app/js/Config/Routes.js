
'use strict';
angular.module('AngStarter')
.run(['$rootScope', '$transitions', '$state', '$stateParams', '$location', '$templateCache', '$cookies',
function ($rootScope, $state, $stateParams, $transitions, $location, $templateCache, $cookies) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

  });

  $rootScope.$on('$stateChangeSuccess', function (e, toState, toParams, fromState, fromParams) {

    var session = null;

    try {
      session = JSON.parse($cookies.get('dataLogin')).Response;
      // console.log('currentUser : ', session);
    } catch (e) {}

    // now, redirect only not authenticated
    console.log(toState.url)

    if ( session == null || session == undefined) {
      e.preventDefault(); // stop current execution
      $location.path('/login'); // go to login

      if (toState.url == "/login" || toState.url == "/") {
        // do nothing
      } else {
        $.smallBox({
          title: "Access Denied!",
          content: "<i>You not allowed to access this page!</i>",
          color: "#a65858",
          iconSmall: "fa fa-times fa-2x fadeInRight animated",
          timeout: 4000
        });
      }
    }
  });
}
])


.config(
    ['$stateProvider', '$urlRouterProvider','JQ_CONFIG', 'MODULE_CONFIG',
      function ($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {

        $urlRouterProvider
          .otherwise('/login');
        $stateProvider
        .state('/', {
          url: '/',
          cache: false,
          controller: function validate($state, $cookies) {
            if ($cookies.get('dataLogin') != undefined) {
              $state.go('main.dashboard');
            } else {
              $state.go('login');
            }
          }
        })
        .state('login', {
            url: '/login',
            cache: false,
            templateUrl: 'partials/Login/Login.html',
            resolve: load(['oitozero.ngSweetAlert','js/Controllers/LoginCtrl.js', 'ui.select2'])
            //  resolve: {
            //     deps: function ($ocLazyLoad, $injector) {
            //         return $ocLazyLoad.load([
            //             'Controllers/LoginCtrl.js'
            //         ])
            //     },
            // }
          })
          .state('main', {
            url: '/main',
            cache: false,
            templateUrl: 'partials/main_panel/main_panel.html',
            resolve: load(['oitozero.ngSweetAlert','js/Controllers/MainCtrl.js'])
          })
          .state('main.dashboard', {
            url: '/dashboard',
            cache: false,
            templateUrl: 'partials/main_panel/dashboard.html',
            resolve: load(['cgBusy', 'js/Controllers/DashboardCtrl.js'])
            // controller: function direct($state) {
            //   var dataLogin = JSON.parse(localStorage.dataLogin);

            //   if (dataLogin.secure[0].id_rekanan) {
            //     $state.go('main.pengumuman_lelang_vendor');
            //   }
            // }
          })
          .state('main.pertanyaan', {
            url: '/pertanyaan',
            cache: false,
            templateUrl: 'partials/Pertanyaan/Question.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert','js/Controllers/PertanyaanCtrl.js'])

          })
          .state('main.list_pekerjaan', {
            url: '/pekerjaan',
            cache: false,
            templateUrl: 'partials/pekerjaan/pekerjaan.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert','js/Controllers/PekerjaanCtrl.js', 'ui.select2'])

          })
          .state('main.cap_user', {
            url: '/cap_user',
            cache: false,
            templateUrl: 'partials/Pertanyaan/cap.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert','js/Controllers/capUserCtrl.js'])

          })
          .state('main.viewPertanyaan', {
            url: '/viewPertanyaan',
            cache: false,
            templateUrl: 'partials/Pertanyaan/viewPertanyaan.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert','js/Controllers/ViewPertanyaanCtrl.js'])

          })
          .state('main.report', {
            url: '/report',
            cache: false,
            templateUrl: 'partials/admin/report/cetak.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert','js/Controllers/admin/ReportCtrl.js'])

          })

          .state('main.PertanyaanTemp', {
            url: '/PertanyaanTemp',
            cache: false,
            templateUrl: 'partials/Pertanyaan/Pertanyaantemp.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert','js/Controllers/PertanyaanTempCtrl.js'])

          })

          .state('main.masterpertanyaan', {
            url: '/masterpertanyaan',
            cache: false,
            templateUrl: 'partials/admin/masterpertanyaan/masterpertanyaan.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert','js/Controllers/admin/masterpertanyaanCtrl.js'])

          })

          .state('main.airport', {
            url: '/airport',
            cache: false,
            templateUrl: 'partials/admin/airport/airport.html',
            resolve: load(['cgBusy', 'js/Controllers/admin/AirportCtrl.js'])

          })
          .state('main.list_user', {
            url: '/list_user',
            cache: false,
            templateUrl: 'partials/admin/user/listuser.html',
            resolve: load(['cgBusy', 'js/Controllers/admin/userCtrl.js'])

          })

          .state('main.list_cap_atc', {
            url: '/cap_atc',
            cache: false,
            templateUrl: 'partials/admin/ATC/CapAtc.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert', 'js/Controllers/admin/CapAtcCtrl.js'])

          })
          .state('main.list_cap_afis', {
            url: '/cap_afis',
            cache: false,
            templateUrl: 'partials/admin/AFIS/CapAfis.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert', 'js/Controllers/admin/CapAfisCtrl.js'])

          })
          .state('main.list_pengajuan_afis', {
            url: '/list_pengajuan_afis',
            cache: false,
            templateUrl: 'partials/admin/AFIS/Afis.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert', 'js/Controllers/admin/AfisCtrl.js'])

          })
          .state('main.manage_user', {
            url: '/manage_user',
            cache: false,
            templateUrl: 'partials/admin/MUser/listuser.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert', 'js/Controllers/admin/MuserCtrl.js', 'ui.select2'])

          })
          .state('main.list_pengajuan_atc', {
            url: '/list_pengajuan_atc',
            cache: false,
            templateUrl: 'partials/admin/ATC/Atc.html',
            resolve: load(['cgBusy','oitozero.ngSweetAlert', 'js/Controllers/admin/AtcCtrl.js'])

          })

        function load(srcs, callback) {
          return {
            deps: ['$ocLazyLoad', '$q',
              function ($ocLazyLoad, $q) {
                var deferred = $q.defer();
                var promise = false;
                srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                if (!promise) {
                  promise = deferred.promise;
                }
                angular.forEach(srcs, function (src) {
                  promise = promise.then(function () {
                    if (JQ_CONFIG[src]) {
                      return $ocLazyLoad.load(JQ_CONFIG[src]);
                    }
                    angular.forEach(MODULE_CONFIG, function (module) {
                      if (module.name == src) {
                        name = module.name;
                      } else {
                        name = src;
                      }
                    });

                    return $ocLazyLoad.load(name);
                  });
                });

                deferred.resolve();
                return callback ? promise.then(function () {
                  return callback();
                }) : promise;
              }
            ]
          }
        }

      }
    ])
