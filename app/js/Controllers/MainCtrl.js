'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('MainCtrl',
    function (
      $scope,
      $state,
      $cookies,
      NavFactory) {

      $scope.menu_navigasi=[];
      $scope.user={};
      
      let datl;

      try{

        $scope.user= JSON.parse($cookies.get('dataLogin')).Response.user;
        datl = JSON.parse($cookies.get('dataLogin')).Response;


        $scope.userLogin = datl.user.nama;
        $scope.dataLogin = datl;
        $scope.menu_navigasi = JSON.parse($cookies.get(datl.id_token));
        
      } catch (error) {

      }
        
      var ConfigApi = {
        headers : {
          "Content-Type": "application/json",
          "token":datl.id_token,
          "keyInit":datl.user.kode_user
        }
      }

      NavFactory.getMenu(datl.user.tipe_user.kode_tipe_user, ConfigApi).then(function (response) {
        $scope.menu_navigasi = response.data.Response;
      })
      
      // $scope.menu_navigasi = JSON.parse($cookies.get(datl.id_token));
      //$scope.menu_navigasi = JSON.parse($cookies.get(datl.id_token));

      // listen for Online event

      //$scope.doesConnectionExist();
      
      //$scope.initMenu();


      $scope.typeView=function(data){
        $state.go(data);
      }

      //$scope.pageSetup = function(level){
       // console.log("data Login "+angular.toJson(level));
         /* NavFactory.getMenu(level,ConfigApi).then(function (response) {
           $scope.menu_navigasi=response.data.Response;
           console.log("data : "+$scope.menu_navigasi[0].kode_tipe_user);
         }); */
        //console.log("menu "+$cookies.get(level));
        
      //}
     
     // $scope.pageSetup(datl.id_token);
      $scope.isNull=function(id){
        if(id.length==0){
          return false;
        }
        return false;
      }





     $scope.logout=function(){
        $.SmartMessageBox({
            title: "Warning!",
            content: "Apa <strong>" + $scope.userLogin + "</strong> yakin untuk Keluar ?",
            buttons: '[Ya][Batal]'
        }, function (ButtonPressed) {
            if (ButtonPressed === 'Ya') {
              $cookies.remove(datl.id_token);
              $cookies.remove('dataLogin');
              
              $state.go("login");

              return true;
            } else if(ButtonPressed === 'Batal') {

            }
        });
     }

    }
  )
