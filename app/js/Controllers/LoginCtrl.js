"use strict";
angular.module('AngStarter')
    .controller('LoginCtrl', function (
        $state,
        loginFactory,
        $scope,
        $cookies,
        NavFactory,
        $uibModal,
        $rootScope,
        AirportFactory,
        RegisterFactory) {
          $scope.listAirport=[];
          $scope.page = 1;
          $scope.displayItems = $scope.listAirport.slice(0, 3);
          $scope.pageChanged = function() {
              var startPos = ($scope.page - 1) * 3;
          };

         var ConfigApi = {
                    headers : {
                        "Content-Type": "application/json",
                        "token":"",
                        "keyInit":""
                    }
                }
        $scope.User = {};
        $scope.u={};
        $scope.select_area={};
        $scope.airportList= [];
        $scope.tipes = [{"id":1,"value":'ATC'},{"id":2,"value":'AFIS'},{"id":3,"value":'FSS'}];



        $scope.Login=function(data){
            Pace.start()
            loginFactory.login(data,ConfigApi).then(function (response) {
             var d=response.data;
             
                if(d.Success){
                    $cookies.put('dataLogin', JSON.stringify(d))
                    ConfigApi.headers.token=d.Response.id_token;
                    ConfigApi.headers.keyInit = d.Response.user.kode_user;
                    //if(data.Result.secure[0].id_rekanan)
                      //       $state.go('main.pengumuman_lelang_vendor');
                        // else
                    // NavFactory.getMenu(d.Response.user.tipe_user.kode_tipe_user, ConfigApi).then(function (response) {
                    // //$scope.menu_navigasi = response.data.Response;
                    //     var dd = d.Response.id_token;
                    //     var menu = response.data.Response;
                    //     $cookies.put(dd, JSON.stringify(menu));
                       
                    // });
                            $state.go("main.dashboard");

                        $.smallBox({
                            title: "Success!",
                            content: "<i class='fa fa-checklist'></i> <i>" + response.data.Info + ".</i>",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });
                        Pace.stop();
                }else{
                    $.smallBox({
                        title: "Warning!",
                        content: "<i>" + response.data.Info + ".</i>",
                        color: "#a65858",
                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    Pace.stop();
                }
            })

        }


        $scope.registerOpenModal=function(){
            $scope.u = {};

            AirportFactory.getAllAirportClient().then((result) => {
                $scope.airportList = result.data.Response;

                $uibModal.open({
                    animation: true,
                    scope:$scope,
                    backdrop  : 'static',
                    templateUrl: 'partials/signup/signup.html',
                    controller: function ($scope,$uibModalInstance) {
                        // $scope.Register = function (value) {
                        //     console.log("adadad "+angular.toJson(value));
                        // $scope.Registered(value);
                        //     $uibModalInstance.dismiss('cancel');
                        // },
    
                        $rootScope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    },
                    size: 'lg'
                })
            })
        }

        $scope.validasi=function(data){
            // Pace.start();
            // $.smallBox({
            //     title: "Success!",
            //     content: "<i class='fa fa-checklist'></i> <i>" + "TOlong Pastikan Semua Field terisi"+ ".</i>",
            //     color: "#659265",
            //     iconSmall: "fa fa-check fa-2x fadeInRight animated",
            //     timeout: 4000
            // });
            // Pace.stop();
            return true;
        }
        $scope.Registered = function(data){
            if ($scope.u.ats_aprovided1){
                $scope.u.ats_aprovided1=1;
            }else{
                $scope.u.ats_aprovided1 = 0;
            }
            if ($scope.u.ats_aprovided2) {
                $scope.u.ats_aprovided2 = 1;
            } else {
                $scope.u.ats_aprovided2 = 0;
            }
            if ($scope.u.ats_aprovided3) {
                $scope.u.ats_aprovided3 = 1;
            } else {
                $scope.u.ats_aprovided3 = 0;
            }
            if ($scope.u.ats_aprovided4) {
                $scope.u.ats_aprovided4 = 1;
            } else {
                $scope.u.ats_aprovided4 = 0;
            }
            if ($scope.u.ats_aprovided5) {
                $scope.u.ats_aprovided5 = 1;
            } else {
                $scope.u.ats_aprovided5 = 0;
            }

            $scope.u.airport_id = JSON.parse($scope.u['airport']).airport_id;
            $scope.u.area = $scope.u.select_areas;

            RegisterFactory.save($scope.u,ConfigApi).then(function(res){
                Pace.start();
                if(res.data.Success){
                    $.smallBox({
                        title: "Success!",
                        content: "<i class='fa fa-checklist'></i> <i>" + res.data.Info + ".</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    $rootScope.cancel();
                    Pace.stop();
                }else{
                    $.smallBox({
                        title: "Warning!",
                        content: "<i>" + res.data.Info + ".</i>",
                        color: "#a65858",
                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    Pace.stop();
                }
            });
        }


      $scope.pilihAirport=function(data){

      }
    });
