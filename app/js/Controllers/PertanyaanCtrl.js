"use strict";
angular.module("AngStarter").
controller('PertanyaanCtrl',function(
  $scope,
  $uibModal,
  Pertanyaanfactory,
  $cookies,
  $state,
  PdfFactory,
  AfisFactory){
$scope.users = [];
$scope.detail=[];
$scope.user=null;
$scope.status={};
$scope.singleSelect=0;
$scope.parent=0;
$scope.disabledParent=false;
$scope.urlGet="http://localhost:8090/api/sertifikasi/preview/";
$scope.selected={
  "m1":false,
  "m2":false,
  "m3":false
};

  $scope.selected1 = {
    "m1": false,
    "m2": false,
    "m3": false
  };
$scope.dataRequest=[];

$scope.fileParent=[];
$scope.fileChild = [];
$scope.ActionDetail={};
$scope.remaksChild="";
$scope.remaksParent = "";
$scope.sizeDetail=0;

// if($scope.users.length<1){
//   alert("paras"+$scope.users.length);
//
// }
var datl=JSON.parse($cookies.get('dataLogin')).Response;
 var ConfigApi = {
            headers : {
                "Content-Type": "application/json",
                "token":datl.id_token,
                "keyInit":datl.user.kode_user
            }
        }


  var ConfigApi2 = {
    headers: {
      "Content-Type": "application/json",
      "token": datl.id_token,
      "keyInit": datl.user.kode_user,
      "email": datl.user.kode_user
    }
  }
        var ConfigApiUp = {
          headers : {
              "Content-Type": undefined,
              "token":datl.id_token,
                "keyInit":datl.user.kode_user
          }
      }

      $scope.check=function(){
        var d={
          "type":datl.user.area,
          "user":datl.user.kode_user,
          "status":11

        };

        AfisFactory.getStatusPertanyaan(d,ConfigApi2).then(function(res){

          var r=res.data.Response;

          if(r>0){
            $state.go("main.viewPertanyaan");
          }else{
            $scope.getPertanyaanBYKodeArea(d);
          }
        })
      }

 $scope.getPertanyaanBYKodeArea= function (kode) {
  Pertanyaanfactory.getPertanyaan(kode.type,ConfigApi).then(function(data) {
            $scope.users = data.data['Response'];
          })
   console.log("data per "+angular.toJson($scope.users));
  }

  
//uploadFile


$scope.updateDataLampiran2 = function(file) {
      var fd = new FormData();
    //Take the first selected file
    var index=$scope.index;
     for(var i=0;i<file.length;i++){
       fd.append("fileUpload", file[i]);
     }
     Pertanyaanfactory.upload(fd,ConfigApiUp).then(function(res){
       var data = res.data.Response;
       console.log(data);
       $scope.fileChild = angular.copy(data || []).map(function (val) {
         return {
           "idx": val.idx,
           "link_lampiran": val.link_lampiran,
           "path_file": val.path_file
         }
       });
       console.log($scope.fileChild);
     });
  
  }

  $scope.updateDataLampiran = function(file) {
      var fd = new FormData();
    //Take the first selected file
     for(var i=0;i<file.length;i++){
       fd.append("fileUpload", file[i]);
     }

     Pertanyaanfactory.upload(fd,ConfigApiUp).then(function(res){
      var data = res.data.Response;
      console.log(data);
       $scope.fileParent =angular.copy(data || []).map(function (val) {
         return {
               "idx":val.idx,
               "link_lampiran": val.link_lampiran,
               "path_file": val.path_file
         }
       });
       console.log($scope.fileParent);
     })
   
  }

  // $scope.PostData=function(d1){
  //   $scope.renderRemaks(d1);

  //   //console.log("data 1 "+angular.toJson(d1)+" data 2 "+angular.toJson($scope.detail));
  //    Pertanyaanfactory.save($scope.dataPost,ConfigApi).then(function(res){
  //      var data = res.data;
  //      if(data.Success){
  //        $.smallBox({
  //          title: "Success!",
  //          content: "<i class='fa fa-checklist'></i> <i>" +"Data Berhasil Di Simpan" + ".</i>",
  //          color: "#659265",
  //          iconSmall: "fa fa-check fa-2x fadeInRight animated",
  //          timeout: 4000
  //      });
  //      $state.go('main.dashboard');
  //      }
  //      console.log(data);
  //     })

  // }

  // $scope.renderRemaksDetail=function(data){
  //     $scope.dataPost[$scope.index].detail=data;
  //     console.log("data mateng nii "+angular.toJson($scope.dataPost));
  // }
  // $scope.renderRemaks=function(data){
  //   for(var i=0;i<data.length;i++){
  //     $scope.dataPost[i].remarks=data[i].remarks;
  //   }
  //   console.log("data akhir"+angular.toJson($scope.dataPost));

  // }



// $scope.renderDetailnya=function(data){
//   $scope.detail=angular.copy(data||[]).map(function(val) {
//     return{
//           "kode_detail_pq":val.kode_detail_pq,
//           "kode_kategory_pq":val.kategory.kode_kategory_pq,
//           "kd_area":val.area.kd_area,
//           "detail_pq":val.detail_pq,
//             "status_detail_pq":3,
//             "remaks":"",
//             "file_upload":[
//               {
//                 "idx":-1,
//                 "link_lampiran":"",
//                 "path_file":""
//                 },
//                 {
//                   "idx":-1,
//                   "link_lampiran":"",
//                   "path_file":""
//                   }
//           ]
//         }
//       })
// console.log("detaillll "+angular.toJson($scope.detail));
// }

  $scope.PostPertanyaan=function(pq_number,data){
    var dataPost =  {
      "file_upload":angular.copy($scope.fileParent || []).map(function (val) {
          return {

            "idx": val.idx,
            "link_lampiran": val.link_lampiran,
            "path_file": val.path_file
         }
        }),
        "detail":[],
      "airport_id": datl.user.airport.airport_id,
      "kode_area": datl.user.area,
        "pq_number": pq_number,
        "remaks_admin": "",
      "remarks": data,
        "status_pq": 3
      }

        if($scope.selected1.m1 ){
          dataPost.status_pq = 1;
        } else if($scope.selected1.m2 ){
          dataPost.status_pq = 2;
        }else{
          dataPost.status_pq=3;
        }
    dataPost.detail=$scope.dataRequest;
    console.log("hasil akhir " + angular.toJson(dataPost));
     AfisFactory.saveSingle(dataPost, ConfigApi).then(function (res) {

      console.log("hasil dari server : "+angular.toJson(res.data));
       if (res.data.Success){
         $scope.Notif(res.data.Info);
       }
    });
  
      
  }

  $scope.open=function(pq_number){

          // alert($scope.detail[0].area.kd_area);
            $uibModal.open({
              animation: true,
              scope:$scope,
              backdrop  : 'static',
              templateUrl: 'partials/Pertanyaan/QuestionDetail.html',
              controller: function ($scope, $http,$uibModalInstance) {
                $scope.ok = function () {
                  $scope.PostPertanyaan(pq_number, $scope.remaksParent);
                  $scope.netralParent();
                  $uibModalInstance.dismiss('cancel');
                };

                $scope.cancel = function () {
                  $scope.netralParent();
                  $uibModalInstance.dismiss('cancel');
                };
              },
              size: 'lg'
            })
  }

  $scope.Notif=function(data){
    Pace.start();
    $.smallBox({
        title: "Info!",
        content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
        color: "#659265",
        iconSmall: "fa fa-check fa-2x fadeInRight animated",
        timeout: 4000
    });
    Pace.stop();
  }

  $scope.renderDataChild = function (key, temp) {
    console.log("aaaakhir ============= "+temp+"  "+key);
    let index = $scope.dataRequest.findIndex(ex => ex.kode_detail_pq === key);
    $scope.dataRequest[index].remaks=temp;
    var temp =1;
    if($scope.selected.m2){
      temp=2;
    } else if ($scope.selected.m3){
      temp=3
    }
    $scope.dataRequest[index].file_upload=angular.copy($scope.fileChild|| []).map(function (val) {
      return {
        "idx": val.idx,
        "link_lampiran": val.link_lampiran,
        "path_file": val.path_file
      }
    });
    $scope.dataRequest[index].status_detail_pq=temp;
    console.log("ini data post nya : "+angular.toJson($scope.dataRequest));
  }
  $scope.netralParent = function () {
    $scope.selected1 = {
      "m1": false,
      "m2": false,
      "m3": false
    };
    $scope.fileParent = null;
    $scope.remaksParent="";
  }
  $scope.setValueParent = function (data, file) {
    $scope.remaksParent = data.remarks;
    $scope.fileParent = file;
    switch (data.status_pq) {
      case 1:
        $scope.selected1.m1 = true;
        break;

      case 2: $scope.selected1.m2 = true;

        break;
      case 3: $scope.selected1.m3 = true;

        break;
    }
  }

  $scope.openModal = function (data) {
        Pertanyaanfactory.getDetailBy(data.pq_number,data.kode_area,ConfigApi).then (function(res){
          var d=res.data;
            $scope.detail=d.Response;
            if(d.jawaban!=null){
              $scope.setValueParent(d.jawaban,d.file);
              /* if (d.Response.length > 0) {
                $scope.disabledParent = true;
              }else{
                $scope.disabledParent = false;
              } */
            }
          $scope.dataRequest=
            angular.copy($scope.detail || []).map(function (val) {
            return {
              
                  "file_upload": [],
                  "kode_detail_pq": val.kode_detail_pq ,
                  "kode_kategory_pq": val.kategory.kode_kategory_pq,
                  "remaks": "",
                  "status_detail_pq": 3
            }
          })
          console.log(angular.toJson(d));
          
            $scope.open(data.pq_number);
        });

  }


  $scope.isSubbmit=function(){
    var d = {
      "type": datl.user.area,
      "user": datl.user.kode_user
    };
    AfisFactory.isUserWrite(d, ConfigApi).then(function (res) {
      var r = res.data.Response;
      console.log("dari chechk " + angular.toJson(r));
      return r;
  });
}

  $scope.submitToadmin=function(){
    var jml = $scope.isSubbmit();
    if (42==42){//42 kiri di ganti dengan jml,ini test dev
      var d={
        "kode_area":datl.user.area,
        "kode_user":datl.user.kode_user,
        "status":1

      };
      Pertanyaanfactory.submitToadmin(d,ConfigApi).then(function(res){
          $scope.Notif(res.data.Info);
        });
    }else{
      $scope.Notif("Maaf   Masih Ada Pertanyaan yg Harus di Isi.");
    }
  }



  // $scope.openDetailBy = function (data) {
  //   var d = { 'pq_number': data.pq.pq_number, 'kode_user': data.kode_user };
  //   var pilihan = [
  //     '-',
  //     'Tidak Diterapkan',
  //     'Sesuai',
  //     'Tidak Sesuai'
  //   ];
  //   $scope.header = data.pq.pq_number;
  //   AfisFactory.getListDetailBy2(d, ConfigApi).then(function (res) {
  //     if (res.data.Response.length > 0) {
  //       $scope.AfisDetailBy = res.data.Response;
  //       //  console.log("detail "+angular.toJson($scope.AfisDetailBy));
  //       $uibModal.open({
  //         animation: true,
  //         templateUrl: 'partials/admin/ATC/AtcDetail.html',
  //         scope: $scope,
  //         backdrop: 'static',
  //         keyboard: false,
  //         controller: function ($uibModalInstance, $scope) {
  //           $scope.closeModalDocDetail = function () {
  //             $uibModalInstance.dismiss('cancel');
  //           },

  //             $scope.translate = function (s) {
  //               return pilihan[s];
  //             };
  //         },
  //         size: 'lg'
  //       });
  //     } else {
  //       $scope.notif("tidka ada Detail", false);
  //     }
  //   });
  // }

$scope.netralChild=function(){
  $scope.selected = {
    "m1": false,
    "m2": false,
    "m3": false
  };
  $scope.fileChild=null;
  console.log("child final : " + angular.toJson($scope.dataRequest));
  $scope.remaksChild="";
}

 

 
  $scope.setValueChild=function(data){
    let index = $scope.dataRequest.findIndex(ex => ex.kode_detail_pq === data.kode_detail_pq);
    $scope.remaksChild = $scope.dataRequest[index].remaks;
    $scope.fileChild = $scope.dataRequest[index].file_upload;
    switch ($scope.dataRequest[index].status_detail_pq) {
      case 1:
        $scope.selected.m1 = true;
        break;

      case 2: $scope.selected.m2 = true;

        break;
      case 3: $scope.selected.m3 = true;

        break;
    }
  }

  $scope.chache=function(key){
    if ($scope.dataRequest.length>0){
      $scope.sizeDetail = $scope.dataRequest.length;
      let index = $scope.dataRequest.findIndex(ex => ex.kode_detail_pq === key);
      if ($scope.dataRequest[index].file_upload.length >0 ||
         $scope.dataRequest[index].remaks.length > 0 || 
        $scope.dataRequest[index].status_detail_pq!=3 ){
          console.log("ada di chache "+angular.toJson( $scope.dataRequest[index]));
        return true;
    }
  }
    return false;
  }



  $scope.openModalActionDetail = function (data) {
    if ($scope.chache(data.kode_detail_pq)){
      $scope.setValueChild(data);
      $uibModal.open({
        animation: true,
        templateUrl: 'partials/Pertanyaan/QuestionActionDetail.html',
        scope: $scope,
        backdrop: 'static',
        keyboard: false,
        controller: function ($uibModalInstance, $scope) {
          $scope.cancel = function () {
            $scope.netralChild();
            $uibModalInstance.dismiss('cancel');
          },

            $scope.simpan = function () {
            $scope.renderDataChild(data.kode_detail_pq, $scope.remaksChild);
            $scope.netralChild();
              $uibModalInstance.dismiss('cancel');
            };
        },
        size: 'lg'
      });
    }else{
      $scope.loadonDatabase(data);
    }
  }
  
$scope.loadonDatabase=function(data){
    var req={
      "airport_id": datl.user.airport.airport_id,
      "kode_area": data.area.kd_area,
      "kode_kategory_pq": data.kategory.kode_kategory_pq,
      "kode_detail_pq": data.kode_detail_pq,
      "kode_user": datl.user.kode_user,
      "pq_number": data.pq_number
    };
    //console.log("data req detail question " + angular.toJson(req));

    AfisFactory.questionDetailBy(req, ConfigApi).then(function (res) {
      console.log("ondatabase =========================");
      if (res.data.Response!=null) {
        $scope.sizeDetail = res.data.Response.length;
        $scope.ActionDetail = res.data.Response;
        $scope.key = data.kode_detail_pq;
        $scope.remaksChild = $scope.ActionDetail.remarks;
        $scope.fileChild = res.data.file;
        switch ($scope.ActionDetail.status_detail_pq) {
          case 1:
            $scope.selected.m1 = true;
            break;

          case 2: $scope.selected.m2 = true;

            break;
          case 3: $scope.selected.m3 = true;

            break;
        }
      }
        console.log("detail " +angular.toJson(res.data));
        $uibModal.open({
          animation: true,
          templateUrl: 'partials/Pertanyaan/QuestionActionDetail.html',
          scope: $scope,
          backdrop: 'static',
          keyboard: false,
          controller: function ($uibModalInstance, $scope) {
            $scope.cancel = function () {
              $scope.netralChild();
              $uibModalInstance.dismiss('cancel');
            },

              $scope.simpan = function () {
              $scope.renderDataChild(data.kode_detail_pq,$scope.remaksChild);
              $scope.netralChild();
              $uibModalInstance.dismiss('cancel');
              };
          },
          size: 'lg'
        });
    });
  }


  $scope.previewFile=function(data){
    PdfFactory.preview(data,ConfigApi).then(function(res){
   if (res.data==null){
      $scope.Notif("Terjadi Kesalahan pada Server.");
    }    
  });
}

});
