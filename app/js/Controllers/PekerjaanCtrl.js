"use strict";
angular.module("AngStarter").
controller('PekerjaanCtrl',function($scope, $uibModal, $cookies, PekerjaanFactory, $rootScope){

  var ConfigApi= {
    headers: {
      "Content-Type": "application/json",
      "token": JSON.parse($cookies.get('dataLogin')).Response.id_token,
      "keyInit": JSON.parse($cookies.get('dataLogin')).Response.user.kode_user
    }
  }
  
  $scope.dataPekerjaan = [], $scope.listPic = [];
  $scope.page = 1;
  $scope.dataAssign = {}

  $scope.render = function(){
    $rootScope.promiseBusy = PekerjaanFactory.getAll(ConfigApi).then((result) => {
      var data = angular.copy(result.data.Response || []).map(function(item){
        return {
          "airport": `${item.user.airport.airport_name} / ${item.user.airport.city}`,
          "kode_prog": item.kode_prog,
          "pengaju_name": item.user.nama,
          "pengaju_email": item.user.email,
          "pengaju_telp": item.user.telp_kantor,
          "kode_user": item.user.kode_user,
          "status": item.status,
          "pic": item.pic == '-' ? '' : item.pic
        }
      });

      $scope.dataPekerjaan = data;
      
    })
  }

  $scope.render();

  $scope.assignTask = function(data) {
    $rootScope.promiseBusy = PekerjaanFactory.getPic('ADM', ConfigApi).then((result) => {
      
      $scope.dataAssign = data;
      $scope.dataAssign._pic = data.pic.kode_user;
      $scope.listPic = result.data.Response;

      $uibModal.open({
        animation: true,
        templateUrl: 'partials/pekerjaan/pekerjaan_assign.html',
        scope: $scope,
        backdrop: 'static',
        keyboard: false,
        controller: function ($uibModalInstance) {
          $rootScope.closeModal = function () {
            $uibModalInstance.dismiss('cancel');
          };
        },
        size: 'md'
      });
    })
  }

  $scope.save = function(data) {
    var param = {
      "kode_prog": data.kode_prog,
      "kode_user": data.kode_user,
      "pic": data._pic,
      "status": data.status,
    }

    PekerjaanFactory.save(param, ConfigApi).then((result) => {
      if (result.data.Success) {

        $.smallBox({
            title: "Success!",
            content: "<i>" + result.data.Info + "...</i>",
            color: "#659265",
            iconSmall: "fa fa-check bounce animated",
            timeout: 4000
        });

        $rootScope.closeModal()
        $scope.render();
    } else {
        $.smallBox({
            title: "Failed Save Data!",
            content: "<i>" + result.data.Info + "...</i>",
            color: "#a65858",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
    })

  }
  
})