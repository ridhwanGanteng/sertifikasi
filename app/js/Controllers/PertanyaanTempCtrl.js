"use strict";
angular.module("AngStarter").controller('PertanyaanTempCtrl',function(
  $scope,
  $http,
  $uibModal,
  Pertanyaanfactory,
  $cookies,
  $state,
  SweetAlert,
  RegisterFactory,
  AfisFactory){
$scope.users = [];
$scope.dataPost=[];
$scope.detail=[];
$scope.user=null;
$scope.names = [{"id":1,"value":"Sesuai"},{"id":2,"value":"Tidak Sesuai"},{"id":3,"value":"Belum Ditentukan"}];
$scope.status={};
$scope.singleSelect=0;
$scope.index=0;
$scope.index2=0;
$scope.parent=0;
$scope.file=[];


$scope.AfisDetail=[];
$scope.page = 1;
$scope.displayItems = $scope.AfisDetail.slice(0, 6);
 $scope.pageChanged = function() {
    var startPos = ($scope.page - 1) * 6;
  };


var datl=JSON.parse($cookies.get('dataLogin')).Response;
 var ConfigApi = {
            headers : {
                "Content-Type": "application/json",
                "token":datl.id_token,
                "keyInit":datl.user.kode_user
            }
        }
        var ConfigApiUp = {
          headers : {
              "Content-Type": undefined,
              "token":datl.id_token,
                "keyInit":datl.user.kode_user
          }
      }

      $scope.check=function(){
        var d={
          "type":datl.user.area,
          "user":datl.user.kode_user
        };
        AfisFactory.isUserWrite(d,ConfigApi).then(function(res){
          var r=res.data.Response;
          if(r==42){
            $scope.getPertanyaan(datl.user.kode_user);
          }else if(r==0){
            $state.go("main.Pertanyaan");
          }else{
            $state.go("main.viewPertanyaan");
          }
        })
      }

 $scope.getPertanyaan= function (d) {
  AfisFactory.getDetailBy(d,ConfigApi).then(function(data) {
            $scope.AfisDetail=data.data.Response;
  });
}




  $scope.updateData = function(s2,index) {
    $scope.dataPost[index].status_pq=s2;
    console.log($scope.dataPost);
    }

    $scope.updateData2 = function(s2,index) {
    $scope.detail[index].status_detail_pq=s2.id;
    }
//uploadFile

$scope.setIndex=function(i){
  $scope.index=i;
}
$scope.setIndex2=function(i){
  $scope.index2=i;
}


$scope.updateDataLampiran2 = function(file) {
      var fd = new FormData();
    //Take the first selected file
    var index=$scope.index;
     for(var i=0;i<file.length;i++){
       fd.append("fileUpload", file[i]);
     }
     Pertanyaanfactory.upload(fd,ConfigApiUp).then(function(res){
      var data = res.data;
      for(var i=0; i<file.length;i++){
                $scope.detail[$scope.index2].file_upload[i].path_file=data.dto[i];
                 $scope.detail[$scope.index2].file_upload[i].idx=i;
                 $scope.detail[$scope.index2].file_upload[i].link_lampiran=data.lampiran[i]
           }
     })
   console.log($scope.detail);
  }

  $scope.updateDataLampiran = function(file) {
      var fd = new FormData();
    //Take the first selected file
    var index=$scope.index;
     for(var i=0;i<file.length;i++){
       fd.append("fileUpload", file[i]);
     }

     Pertanyaanfactory.upload(fd,ConfigApiUp).then(function(res){
      var data = res.data;
      for(var i=0; i<file.length;i++){
                $scope.dataPost[index].file_upload[i].path_file=data.dto[i];
                 $scope.dataPost[index].file_upload[i].idx=i;
                 $scope.dataPost[index].file_upload[i].link_lampiran=data.lampiran[i]
           }
           console.log($scope.dataPost[index]);
     })
   console.log($scope.dataPost);
  }

  $scope.PostData=function(d1){
    $scope.renderRemaks(d1);

    //console.log("data 1 "+angular.toJson(d1)+" data 2 "+angular.toJson($scope.detail));
     Pertanyaanfactory.save($scope.dataPost,ConfigApi).then(function(res){
       var data = res.data;
       if(data.Success){
         $.smallBox({
           title: "Success!",
           content: "<i class='fa fa-checklist'></i> <i>" +"Data Berhasil Di Simpan" + ".</i>",
           color: "#659265",
           iconSmall: "fa fa-check fa-2x fadeInRight animated",
           timeout: 4000
       });
       $state.go('main.dashboard');
       }
       console.log(data);
      })

  }

  $scope.renderRemaksDetail=function(data){
      $scope.dataPost[$scope.index].detail=data;
      console.log("data mateng nii "+angular.toJson($scope.dataPost));
  }
  $scope.renderRemaks=function(data){
    for(var i=0;i<data.length;i++){
      $scope.dataPost[i].remarks=data[i].remarks;
    }
    console.log("data akhir"+angular.toJson($scope.dataPost));

  }



$scope.renderDetailnya=function(data){
  $scope.detail=angular.copy(data||[]).map(function(val) {
    return{
          "kode_detail_pq":val.kode_detail_pq,
          "kode_kategory_pq":val.kategory.kode_kategory_pq,
          "kd_area":val.area.kd_area,
          "detail_pq":val.detail_pq,
            "status_detail_pq":3,
            "remaks":"",
            "file_upload":[
              {
                "idx":-1,
                "link_lampiran":"",
                "path_file":""
                },
                {
                  "idx":-1,
                  "link_lampiran":"",
                  "path_file":""
                  }
          ]
        }
      })
console.log("detaillll "+angular.toJson($scope.detail));
}


  $scope.open=function(){

          // alert($scope.detail[0].area.kd_area);
            $uibModal.open({
              animation: true,
              scope:$scope,
              backdrop  : 'static',
              templateUrl: 'partials/Pertanyaan/modal.html',
              controller: function ($scope, $http,$uibModalInstance) {
                $scope.close = function () {
                  $scope.renderRemaksDetail($scope.detail);
                  $uibModalInstance.close();
                };

                $scope.cancel = function () {
                  $uibModalInstance.dismiss('cancel');
                };
              },
              size: 'lg'
            })
  }

  $scope.Notif=function(data){
    Pace.start();
    $.smallBox({
        title: "Info!",
        content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
        color: "#659265",
        iconSmall: "fa fa-check fa-2x fadeInRight animated",
        timeout: 4000
    });
    Pace.stop();
  }

  $scope.openModal = function (data,index) {
    console.log("akhisr nya "+angular.toJson($scope.detail));
      if( $scope.dataPost[index].detail.length >2){
        data=$scope.detail;
        $scope.open();
      }else{
        Pertanyaanfactory.getDetailBy(data.pq_number,ConfigApi).then (function(res){
          data=res.data.Response;
          $scope.renderDetailnya(data);
          if($scope.detail.length>0){
            $scope.open();
          }else{
          $scope.Notif("Tidak ad Detail nya.");
          }
        });
      }
      $scope.setIndex(index);

  }


  $scope.submitToadmin=function(){
    var d={
      "kode_area":datl.user.area,
      "kode_user":datl.user.kode_user,
      "status":0

    };
    alert();
    Pertanyaanfactory.submitToadmin(d,ConfigApi).then(function(res){
        $scope.Notif(res.data.Info);
      });
  }

});
