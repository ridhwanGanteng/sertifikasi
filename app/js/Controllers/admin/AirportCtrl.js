'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('AirportCtrl',
    function (
      $scope, 
      AirportFactory,
      $rootScope, 
      $location, 
      $state,
       $cookies, 
       $filter,
       $uibModal,
       
       SweetAlert) {

      var ConfigApi, dataLogin, id_role;
      $scope.user= JSON.parse($cookies.get('dataLogin')).Response;
      $scope.airport=[];
      $scope.air={};
      $scope.page = 1;
      $scope.displayItems = $scope.airport.slice(0, 3);
      $scope.pageChanged = function() {
          var startPos = ($scope.page - 1) * 3;
      };

      var ConfigApi = {
        headers : {
            "Content-Type": "application/json",
            "token":JSON.parse($cookies.get('dataLogin')).Response.id_token,
            "keyInit":JSON.parse($cookies.get('dataLogin')).Response.user.kode_user
        }
    }

    $scope.notify=function(data,status,state){
      Pace.start()
      if(status){
        //if(data.Result.secure[0].id_rekanan)
          //       $state.go('main.pengumuman_lelang_vendor');
            // else
                $state.go(state);

            $.smallBox({
                title: "Success!",
                content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                color: "#659265",
                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                timeout: 4000
            });
            Pace.stop();
    }else{
        $.smallBox({
            title: "Warning!",
            content: "<i>" + data + ".</i>",
            color: "#a65858",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 4000
        });
        Pace.stop();
    }
    }

      $scope.getAi = function(){
        console.log("api "+angular.toJson(ConfigApi));
        AirportFactory.getAllAirport(ConfigApi).then(function (response) {
            console.log("paaak "+response.data);
            $scope.airport =response.data.Response;
        })
    }

    $scope.saveAirport = function(data){
      
        AirportFactory.save(data,ConfigApi).then(function (response) {
          $scope.airport =response.data;
          $scope.notify(response.data.Info,response.data.Success,"main.airport")
        })
        
    }

    $scope.openModalEdit = function (data) {
      $scope.ai=data;
        console.log("data ini"+data);
          $scope.air=data;
          // alert($scope.detail[0].area.kd_area);
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/airport/modalEdit.html',
            controller: function ($scope, $http,$uibModalInstance) {
              $scope.ok = function (data) {
                console.log("iniiiii : "+angular.toJson(data));
                data.editor=$scope.user.nama;
                $scope.saveAirport(data);
                $scope.ai={};
                $uibModalInstance.close();
              };
            
              $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })
      };


      $scope.openModalTambah = function () {
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/airport/modalTambah.html',
            controller: function ($scope, $http,$uibModalInstance) {
              $scope.ok = function (data) {
                console.log("iniiiii : "+angular.toJson(data));
                data.creator=$scope.user.nama;
                $scope.saveAirport(data);
                $scope.airport[$scope.airport.length]=data; 
                $scope.ai={};
                $uibModalInstance.close();
              };
            
              $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })
      };
     
 
    });