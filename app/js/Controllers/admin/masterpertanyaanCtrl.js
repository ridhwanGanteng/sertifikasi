'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('MasterCtrl',
    function (
      $scope,
      MasterFactory,
      $rootScope,
      $location,
      $state,
       $cookies,
       $filter,
       $uibModal,

       SweetAlert) {

      var ConfigApi, dataLogin, id_role;

      $scope.user= JSON.parse($cookies.get('dataLogin')).Response;
      $scope.master=[];
      $scope.childData = [];
      $scope.dataEditDetail = {};
      $scope.ma={};
      $scope.da = {};
      $scope.page = 1;
      $scope.title="";
      $scope.area="ATC";
       $scope.displayItems = $scope.master.slice(0, 3);
       $scope.pageChanged = function() {
          var startPos = ($scope.page - 1) * 3;


        };
      var ConfigApi = {
        headers : {
            "Content-Type": "application/json",
            "token":JSON.parse($cookies.get('dataLogin')).Response.id_token,
            "keyInit":JSON.parse($cookies.get('dataLogin')).Response.user.kode_user
        }
    }

    $scope.notify=function(data,status){
      Pace.start()
      if(status){
        //if(data.Result.secure[0].id_rekanan)
          //       $state.go('main.pengumuman_lelang_vendor');
            // else

            $.smallBox({
                title: "Success!",
                content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                color: "#659265",
                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                timeout: 4000
            });
            Pace.stop();
    }else{
        $.smallBox({
            title: "Warning!",
            content: "<i>" + data + ".</i>",
            color: "#a65858",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 4000
        });
        Pace.stop();
    }
    }

    $scope.swipe=function(d){
      $scope.area=d;
        $scope.title="PROTOCOL QUESTION - ("+d+")";
      var data={'type':d};
      MasterFactory.getBy(data,ConfigApi).then(function (response) {
            if(response.data.Response.length>0){
              $scope.master =response.data.Response;
            }else{
              $scope.master=[];
            }
        })
    }

    // update get by id master

    $scope.saveMaster = function(){

      
      var param = {
        "detail": angular.copy($scope.childData || []).map((item) => {
          return {
            "kode_detail_pq": item.kode_detail_pq,
            "kode_kategory": item.kode_kategory_pq,
            "protocol_detail": item.detail_pq
          }
        }),
        "doc_refrence": $scope.ma.doc_refrence,
        "kode_area": $scope.area,
        "pq_number": $scope.ma.pq_number,
        "protocol_question": $scope.ma.protocol_question
      }

      if($scope.ma['creator'] == undefined) {

        MasterFactory.save(param, ConfigApi).then(function (response) {
          if(response.data.Success){
            $scope.swipe($scope.area);
            $scope.notify(response.data.Info,response.data.Success);

            $rootScope.closeTambah();
          } else {
            $.smallBox({
              title: "Warning!",
              content: "<i>" + response.data.Info + ".</i>",
              color: "#a65858",
              iconSmall: "fa fa-times fa-2x fadeInRight animated",
              timeout: 4000
            });
          }
        })
      } else {
        MasterFactory.put(param, ConfigApi).then(function (response) {
          if(response.data.Success){
            $scope.swipe($scope.area);
            $scope.notify(response.data.Info,response.data.Success);

            $rootScope.closeTambah();
          } else {
            $.smallBox({
              title: "Warning!",
              content: "<i>" + response.data.Info + ".</i>",
              color: "#a65858",
              iconSmall: "fa fa-times fa-2x fadeInRight animated",
              timeout: 4000
            });
          }
        })
      }

    }

$scope.BuildpatternParent=function(data){
  $scope.parent=angular.copy(data||[]).map(function(val) {
    return{
        "doc_refrence": "string",
        "kode_area": "string",
        "kode_area_lama": "string",
        "pq_number": "string",
        "protocol_question": "string"

      }
    })
}

$scope.BuildpatternChild=function(data){
  $scope.child=angular.copy(data||[]).map(function(val) {
    return{
      "detail": [
      {
        "kode_detail_pq": "string",
        "kode_kategory": "string",
        "kode_kategory_lama": "string",
        "protocol_detail": "string"
      }
      ]
      }
    })
}



    $scope.openModalEditDetail = function (data) {
    
      MasterFactory.getKategory(ConfigApi).then(function(r){
        $scope.da.kategory=r.data.Response;

        var param = {
          _kode_detail_pq: data.kode_detail_pq,
          _detail_pq: data.detail_pq,
          _kode_kategory_pq: data.kode_kategory_pq,
          index: data.index
        }

        $scope.dataEditDetail = param;
        $uibModal.open({
          animation: true,
          scope:$scope,
          backdrop  : 'static',
          templateUrl: 'partials/admin/masterpertanyaan/modalAddDetail.html',
          controller: function ($scope, $http, $uibModalInstance) {
            $scope.ok = function (data) {
              console.log("iniiiii : "+angular.toJson(data));
              $uibModalInstance.close();
            };

            $rootScope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
            };
          }
        })
      })
    }


    $scope.openModalEdit = function (data) {
        MasterFactory.getDetailBy(data.pq_number,data.area.kd_area,ConfigApi).then(function(res){
            // data.detail=res.data.Response;
            if(res.data.Success){
              $scope.ma = data;

              var arrayChild = res.data.Response || [];

              for (let i = 0; i < arrayChild.length; i++) {
                arrayChild[i].index = i;
                arrayChild[i].kode_kategory_pq = arrayChild[i].kategory.kode_kategory_pq;
              }

              $scope.childData = arrayChild;
              
              $uibModal.open({
                animation: true,
                scope:$scope,
                backdrop  : 'static',
                templateUrl: 'partials/admin/masterpertanyaan/modalEdit.html',
                controller: function ($scope, $http,$uibModalInstance) {
                  $scope.ok = function (data) {
                    console.log("iniiiii : "+angular.toJson(data));
                    $uibModalInstance.close();
                  };

                  $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                  };
                },
                size:'lg'
              })
            } else {
              $.smallBox({
                title: "Warning!",
                content: "<i>" + response.data.Info + ".</i>",
                color: "#a65858",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 4000
              });
            }
            
            
        });
      };


      $scope.openModalTambah = function () {
        $scope.da={};
        $scope.ma={};
        $scope.childData = [];
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/masterpertanyaan/modalTambah.html',
            controller: function ($scope,$uibModalInstance) {
              // $scope.ok = function () {
              //   $scope.ma.kode_area=$scope.area;
              //   $scope.ma.detail=[];
              //   console.log("iniiiii : " + angular.toJson($scope.ma) + "  " + $scope.area);
              //   $scope.saveMaster($scope.ma);
              //   //$scope.master[$scope.master.length]=data;
              //   $scope.mas={};
              //   $uibModalInstance.close();
              // };

              $rootScope.closeTambah = function () {
                $uibModalInstance.dismiss('cancel');
              };
            },
            size:'lg'
          })
      };


$scope.openModalAddDetail=function(){
  // for (let i = 0; i < 41; i++) {
  //   $scope.da[i]= {};
  // }
  $scope.dataEditDetail = {};
 
  MasterFactory.getKategory(ConfigApi).then(function(r){
    $scope.da.kategory=r.data.Response;
  })
  $uibModal.open({
    animation: true,
    scope:$scope,
    backdrop  : 'static',
    templateUrl: 'partials/admin/masterpertanyaan/modalAddDetail.html',
    controller: function ($scope, $http,$uibModalInstance) {
      // $scope.ok = function (data) {
      //   // console.log("iniiiii : "+angular.toJson(data));
      //   $scope.childData.push(data);
      //   $scope.dataEditDetail = {
      //     kode_detail_pq: '',
      //     detail_pq: '',
      //     kode_kategory_pq: ''
      //   };
      //   console.log($scope.dataEditDetail)
      //   $uibModalInstance.close();
      // };

      $rootScope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  })
}

$scope.pushArrayDetail = function(data) {
  var param = {
    kode_detail_pq: data._kode_detail_pq,
    detail_pq: data._detail_pq,
    kode_kategory_pq: data._kode_kategory_pq,
    index: data.index ? data.index : $scope.childData.length
  }

  if(data.index != undefined) {
    $scope.childData[data.index] = param;
  } else {
    $scope.childData.push(param);
  }
    
  $scope.dataEditDetail = {};

  $rootScope.cancel();
}

$scope.openDelete = function(index) {
  var hapus = confirm('Anda yakin untuk menghapus data ini?');

  if(hapus) $scope.childData.splice(index, 1);
}

    });
