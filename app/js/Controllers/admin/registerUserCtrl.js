'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('RegisterUserCtrl',
    function (
      $scope, 
      RegisterFactory,
      $rootScope, 
      $location, 
      $state,
       $cookies, 
       $filter,
       
       $uibModal,
       
       SweetAlert) {

      var ConfigApi, dataLogin, id_role;
      $scope.list_user=[];
      $scope.air={};
      var ConfigApi = {
        headers : {
            "Content-Type": "application/json"
        }
    }


      $scope.getAllUser = function(){
        RegisterFactory.getAllUser(ConfigApi).then(function (response) {
            console.log("data user"+response.data);
            $scope.list_user =response.data.Response;
        })
    }

    $scope.saveUser = function(){
        AirportFactory.save($scope.air,ConfigApi).then(function (response) {
            $scope.airport =response.data;
        })
    }

    $scope.openModalEdit = function (data) {
        console.log("data ini"+data);
          $scope.air=data;
          // alert($scope.detail[0].area.kd_area);
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/airport/modalEdit.html',
            controller: function ($scope, $http,$uibModalInstance) {
              $scope.ok = function () {
                $uibModalInstance.close();
              };
            
              $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })
      };


      $scope.openModalTambah = function () {

          // alert($scope.detail[0].area.kd_area);
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/airport/modalTambah.html',
            controller: function ($scope, $http,$uibModalInstance) {
              $scope.ok = function () {
                $uibModalInstance.close();
              };
            
              $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })
      };
     
 
    });