'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('AtcCtrl',
    function (
      $scope,
      AfisFactory,
      $rootScope,
      $location,
      $state,
       $cookies,
       $filter,
       $uibModal,
       SweetAlert,
        AdminFactory) {

      var ConfigApi, dataLogin, id_role;
      $scope.list_p=[];
      $scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(ATC)";
      $scope.header=""
      $scope.mode=0;
      $scope.header2="";
      $scope.names=[{'id':0,'val':'Reject'},{'id':1,'val':'Approve'}];
      $scope.steps=[];
      

      $scope.AfisDetail=[];
      var ConfigApi = {
        headers : {
            "Content-Type": "application/json",
            "token":JSON.parse($cookies.get('dataLogin')).Response.id_token,
            "keyInit":JSON.parse($cookies.get('dataLogin')).Response.user.kode_user,
            "email":""
        }
      }

      $scope.page = 1;
             $scope.displayItems = $scope.AfisDetail.slice(0, 3);
             $scope.pageChanged = function() {
                var startPos = ($scope.page - 1) * 3;
                };
      $scope.SubmitProg=function(data){
        var param ={
          "pq_number":data[0].kode_area,
          "status":1,
          "kode_user":data[0].kode_user,
          "status_lama":0,
          "kode_area":data[0].kode_area
        };
        console.log(param);
        AdminFactory.SubmitProg(param,ConfigApi).then(function(res){
          if(res.data.Success){
            $scope.getByInit();
          }
          $scope.notif(res.data.Info,res.data.Success);
        })
      }

    $scope.getBy=function(d,s){
      var data={'type':d,'status':s};
      $scope.mode=s;
      switch(s){
        case 1:  $scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(ATC)  / (LIST) "; break;
        case 111: $scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(ATC)  / (APPROVED) "; break;
        case -1:$scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(ATC)  / (REJECTED) "; break;
      }
      AfisFactory.getByType(data,ConfigApi).then(function (response) {
            console.log("data user 22"+angular.toJson(response.data.Response));
            if(response.data.Response.length>0){
              $scope.list_p =response.data.Response;
            }else{
              $scope.list_p=[];
              $scope.AfisDetail=[];
            }
        })
    }


    $scope.getByInit=function(){
      $scope.getBy('ATC',1);
    }
    $scope.getByInit();

    $scope.showDetail = function (data) {
      console.log("masuk");
      ConfigApi.headers.email = data.user.kode_user;
       $scope.header2='Data dari Airport '+data.user.airport.airport_name+' - Kota : '+data.user.airport.city+' - Kepulauan : '+data.user.airport.island+' - Oleh : '+data.user.nama;
      AfisFactory.getDetailBy(data.kode_prog, data.user.airport.airport_id,ConfigApi).then(function(res){
          $scope.AfisDetail=res.data.Response;
          console.log("data ini"+angular.toJson($scope.AfisDetail));
        $scope.getStep(data.user.kode_user);
        
        });
      }


      $scope.openDetailBy=function(data){
        var d={'pq_number':data.pq.pq_number,'kode_user':data.kode_user};
        var pilihan = [
          '-',
          'Tidak Diterapkan',
          'Sesuai',
          'Tidak Sesuai'
        ];
          $scope.header=data.pq.pq_number;
        AfisFactory.getListDetailBy2(d,ConfigApi).then(function(res){
          if(res.data.Response.length>0){
            $scope.AfisDetailBy=res.data.Response;
          //  console.log("detail "+angular.toJson($scope.AfisDetailBy));
            $uibModal.open({
                animation: true,
                templateUrl: 'partials/admin/ATC/AtcDetail.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                controller: function ($uibModalInstance, $scope) {
                    $scope.closeModalDocDetail = function () {
                        $uibModalInstance.dismiss('cancel');
                    },

                    $scope.translate=function(s){
                      return  pilihan[s];
                    };
                },
                size: 'lg'
            });
          }else{
            $scope.notif("tidka ada Detail",false);
          }
        });
      }

      $scope.notif=function(data,status){
        Pace.start()
        if(status){
              $.smallBox({
                  title: "Success!",
                  content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                  color: "#659265",
                  iconSmall: "fa fa-check fa-2x fadeInRight animated",
                  timeout: 4000
              });
        }else{
          $.smallBox({
              title: "Warning!",
              content: "<i>" + data + ".</i>",
              color: "#a65858",
              iconSmall: "fa fa-times fa-2x fadeInRight animated",
              timeout: 4000
          });
        }
          Pace.stop();
      }

      $scope.openSPDA_Modal = function (data) {
          $scope.headerModal = {
            noProtocol: data.pq.pq_number,
          }
        AfisFactory.getDetailDocBy(data.id_pq,ConfigApi).then(function(res){
            if(res.data.Response.length>0){
                $scope.AfisDetailDoc=res.data.Response;
                $uibModal.open({
                    animation: true,
                    templateUrl: 'partials/admin/AFIS/formUpload.html',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false,
                    controller: function ($uibModalInstance, $scope) {
                        $scope.closeModalDocDetail = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    },
                    size: 'md'
                });
             }else{
                $scope.notif("Tidak Ada Lampiran");
             }
      });
      }


      $scope.openSPDA_ModalDetail = function (kode,airport) {
          $scope.headerModal = {
            noProtocol: kode+"  - Airport : "+airport,
          }
        AfisFactory.getDetailDocByDetail(kode,airport,ConfigApi).then(function(res){
            if(res.data.Response.length>0){
                $scope.AfisDetailDoc=res.data.Response;
                $uibModal.open({
                    animation: true,
                    templateUrl: 'partials/admin/ATC/formUpload.html',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false,
                    controller: function ($uibModalInstance, $scope) {
                        $scope.closeModalDocDetail = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    },
                    size: 'md'
                });
             }else{
                $scope.notif("Tidak Ada Lampiran");
             }
      });
      }

      $scope.OpenAction=function(data){
        console.log("data action : "+angular.toJson(data));
        $scope.data1={'kode_user':data.kode_user  ,
        'pq_number':data.pq.pq_number,
        'remaks':data.remaks_admin,
        'status':data.status_admin,
        'airport_id':data.airport_id,
        'kode_area':data.kode_area

      };
        if(data.status_admin==1){
          $scope.status_approved="Approved";
        }else if(data.status_admin==2){
          $scope.status_approved="Rejected";
        }else{
          $scope.status_approved="Belum Di Koreksi";
        }

        $uibModal.open({
            animation: true,
            templateUrl: 'partials/admin/AFIS/ApprovOrReject.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            controller: function ($uibModalInstance, $scope) {
                $scope.closeModalDocDetail = function (status,p2) {


                    if(status==1){
                      $scope.data1.status=1;
                      $scope.ApprovOrreject($scope.data1);
                    }else if(status==2){
                      $scope.data1.status=2;
                      $scope.ApprovOrreject($scope.data1);
                    }
                    let index =   $scope.AfisDetail.findIndex( record => record.pq.pq_number=== data.pq.pq_number )
                      $scope.AfisDetail[index].status_admin=$scope.data1.status;
                        $scope.AfisDetail[index].remaks_admin=$scope.data1.remaks;


                    console.log("data modal : "+angular.toJson(p2)+" index"+index);
                    $uibModalInstance.dismiss('close');
                };

            },
            size: 'lg'
        });
      }


      $scope.OpenActionDetail=function(data){
        console.log("data action : "+angular.toJson(data));
        $scope.data1={
        'status':data.status_detail_admin ,
        'status_lama':data.status_detail_admin ,
        'remaks':data.remaks_admin  ,
        'pq_number':data.detail_pq.kode_detail_pq,
        'kode_user':data.kode_user,
        'kode_area':data.kode_area

      };
        if(data.status==1){
          $scope.status_approved="Approved";
        }else if(data.status==2){
          $scope.status_approved="Rejected";
        }else{
          $scope.status_approved="Belum Di Koreksi";
        }

        $uibModal.open({
            animation: true,
            templateUrl: 'partials/admin/ATC/ApprovOrRejectDetail.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            controller: function ($uibModalInstance, $scope) {
                $scope.closeModalDocDetail = function (status,p2) {


                    if(status==1){
                      $scope.data1.status=1;
                    //  $scope.ApprovOrrejectDetail($scope.data1);
                    }else if(status==2){
                      $scope.data1.status=2;
                      //$scope.ApprovOrrejectDetail($scope.data1);
                    }
                    console.log("data modal : "+angular.toJson(p2));
                    $uibModalInstance.dismiss('close');
                };

            },
            size: 'lg'
        });
      }

      $scope.ApprovOrreject=function(data){
        AdminFactory.ApprovOrReject(data,ConfigApi).then(function(res){
          console.log("data "+angular.toJson(res));
            $scope.notif(res.data.Info,res.data.Success);
        });
      }

      $scope.ApprovOrrejectDetail=function(data){
        AdminFactory.ApprovOrRejectDetail(data,ConfigApi).then(function(res){
          console.log("data "+angular.toJson(res));
            $scope.notif(res.data.Info,res.data.Success);
        });
      }


      /*==========================================
                Baru di tambahkan
      */




      $scope.detailAtc=function(d){
        if(d){
          $uibModal.open({
              animation: true,
              templateUrl: 'partials/admin/ATC/AtcPoint2.html',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              controller: function ($uibModalInstance, $scope) {
                  $scope.closeModalDocDetail = function () {
                      $uibModalInstance.dismiss('cancel');
                  }
              },
              size: 'lg'
          });
        }
      }


      $scope.switchStep = function (data,step) {
        let index = (parseInt(step) - 1);
        console.log(index+" data "+data);
        
        if(data){
          $scope.steps[index].step = step;
        }else{
          $scope.steps[index].step = 0;
        }
        $scope.saveStep($scope.steps[index]);
      }

      $scope.getStep = function (data) {
        ConfigApi.headers.email=data;
        AfisFactory.getStep(ConfigApi).then(function (res) {
          if (res.data.Success) {
            $scope.steps = res.data.Response;
          }
          for(let i=0;i<11;i++){
            if($scope.steps[i].step!=0){
              $scope.steps[i].checked=true;
            }
          }
          console.log("step : "+angular.toJson($scope.steps));
        });
      }

      $scope.saveStep = function (data) {
        AfisFactory.saveStep(data, ConfigApi).then(function (res) {
          if (res.data.Success) {
            
          }
        });
      }

    });
