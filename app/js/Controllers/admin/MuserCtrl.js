'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('MUserCtrl',
    function (
      $scope,
      UserFactory,
      RegisterFactory,
      AirportFactory,
      $rootScope,
      $location,
      $state,
       $cookies,
       $filter,
       $uibModal,

       SweetAlert) {

      var ConfigApi, dataLogin, id_role;
      $scope.user= JSON.parse($cookies.get('dataLogin')).Response;
      $scope.list_user=[];
      $scope.us={};
      $scope.status = [{name: "Active", value: 1}, {name: "Inactive", value: 0}];
      $scope.roles = [{name: 'INSPEKTURVA', value: 'INS'},{name: 'ADMIN', value: 'ADM'}, {name: 'USER MONITOR' , value: 'USM'}];
      $scope.airportList = [];


      var ConfigApi = {
        headers : {
            "Content-Type": "application/json",
            "token":JSON.parse($cookies.get('dataLogin')).Response.id_token,
            "keyInit":JSON.parse($cookies.get('dataLogin')).Response.user.kode_user
        }
    }
 $scope.tipes = [{"id":1,"value":'ATC'},{"id":2,"value":'AFIS'},{"id":3,"value":'FSS'}];
 $scope.page = 1;
 $scope.displayItems = $scope.list_user.slice(0, 5);
 $scope.pageChanged = function() {
    var startPos = ($scope.page - 1) * 5;


  };


      $scope.notify=function(data,status){
      Pace.start()
      if(status){
        //if(data.Result.secure[0].id_rekanan)
          //       $state.go('main.pengumuman_lelang_vendor');
            // else
              $scope.getAllUser();
            $.smallBox({
                title: "Success!",
                content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                color: "#659265",
                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                timeout: 4000
            });
            Pace.stop();
    }else{
        $.smallBox({
            title: "Warning!",
            content: "<i>" + data + ".</i>",
            color: "#a65858",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 4000
        });
        Pace.stop();
    }
    };

      $scope.getAllUser = function(){

        $rootScope.promiseBusy = UserFactory.getAllUserBy("ADM",ConfigApi).then(function (response) {

            $scope.list_user = response.data.Response;
            console.log("re"+$scope.list_user);

        })
      }

      $scope.getAllUser();

      $scope.renderMaster = function(){
        AirportFactory.getAllAirportClient().then((result) => {
            $scope.airportList = result.data.Response;
        })
      } 

      $scope.getBy=function(s){
        var data={'kode':s};
        RegisterFactory.getByType(data,ConfigApi).then(function (response) {
              console.log("data user 22"+angular.toJson(response.data.Response));
              if(response.data.Response.length>0){
                $scope.list_user =response.data.Response;
              }else{
                $scope.list_user=[];

              }
          })
      }

      $scope.saveApproved=function(data){
        RegisterFactory.saveApproved(data,ConfigApi).then(function(response){
          $scope.list_user = response.data.Response;
          $scope.notify(response.data.Info,Response.data.Success )
        })
      }

      $scope.saveUser = function(data){

        data['airport'] = JSON.parse(data.airport);
        console.log(data);

        var action = data.kode_user ? 'update' : 'save';

        UserFactory[action](data, ConfigApi).then(function (response) {
          if(response.data.Success){
            $.smallBox({
              title: "Success!",
              content: "<i>" + response.data.Info + "...</i>",
              color: "#659265",
              iconSmall: "fa fa-check bounce animated",
              timeout: 4000
            });
    
            $rootScope.cancel();
            $scope.getAllUser();
          } else {
            $.smallBox({
              title: "Failed Save Data!",
              content: "<i>" + response.data.Info + "...</i>",
              color: "#a65858",
              iconSmall: "fa fa-times fa-2x fadeInRight animated",
              timeout: 4000
            });
          }
        })
      }

      $scope.updateUser = function(data){

          UserFactory.update(data,ConfigApi).then(function (response) {
              $scope.notify(response.data.Info,response.data.Success)
          })
      }

      $scope.openModalEdit = function (data) {
          $scope.us = data;
          // alert($scope.detail[0].area.kd_area);
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/MUser/modalTambah.html',
            controller: function ($scope, $http,$uibModalInstance) {
              // $scope.ok = function (data) {
              //   $scope.updateUser(data);
              //   $uibModalInstance.close();
              // };

              $rootScope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })

          $scope.renderMaster();
        };


      $scope.openModalTambah = function () {
        $scope.us = {};
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/MUser/modalTambah.html',
            controller: function ($scope, $http,$uibModalInstance) {
              // $scope.ok = function (data) {
              //   console.log("iniiiii : "+angular.toJson(data));
              //   data.kode_user=data.email;
              //   data.area="-";
              //   data.tipe_user={
              //     "kode_tipe_user":"ADM"}
              //     ;
              //   $scope.saveUser(data);
              //   $uibModalInstance.close();
              // };

              $rootScope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })

          $scope.renderMaster();
      };
    });
