'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('UserCtrl',
    function (
      $scope,
      UserFactory,
      RegisterFactory,
      $rootScope,
      $location,
      $state,
       $cookies,
       $filter,
       $uibModal,

       SweetAlert) {

      var ConfigApi, dataLogin, id_role;
      $scope.user= JSON.parse($cookies.get('dataLogin')).Response;
      $scope.list_user=[];
      $scope.us={};
     


      var ConfigApi = {
        headers : {
            "Content-Type": "application/json",
            "token":JSON.parse($cookies.get('dataLogin')).Response.id_token,
            "keyInit":JSON.parse($cookies.get('dataLogin')).Response.user.kode_user
        }
    }
 $scope.tipes = [{"id":1,"value":'ATC'},{"id":2,"value":'AFIS'},{"id":3,"value":'FSS'}];
 $scope.page = 1;
 $scope.displayItems = $scope.list_user.slice(0, 3);
 $scope.pageChanged = function() {
    var startPos = ($scope.page - 1) * 3;


  };


      $scope.notify=function(data,status,state){
      Pace.start()
      if(status){
        //if(data.Result.secure[0].id_rekanan)
          //       $state.go('main.pengumuman_lelang_vendor');
            // else
                $state.go(state);

            $.smallBox({
                title: "Success!",
                content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                color: "#659265",
                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                timeout: 4000
            });
            Pace.stop();
    }else{
        $.smallBox({
            title: "Warning!",
            content: "<i>" + data + ".</i>",
            color: "#a65858",
            iconSmall: "fa fa-times fa-2x fadeInRight animated",
            timeout: 4000
        });
        Pace.stop();
    }
    };

    $scope.getBy=function(){
      RegisterFactory.getUserAll(ConfigApi).then(function (response) {
            if(response.data.Response.length>0){
              $scope.list_user =response.data.Response;
            }else{
              $scope.list_user=[];

            }
        })
    }
    $scope.saveApproved=function(data,status){
      data.status_register=status;
      RegisterFactory.saveApproved(data,ConfigApi).then(function(response){
        
        $scope.notify(response.data.Info,response.data.Success,"main.list_user");
        $scope.list_user = response.data.Response;
        $scope.getBy();
      })
    }

    // $scope.saveUser = function(data){
    //
    //     UserFactory.save(data,ConfigApi).then(function (response) {
    //         $scope.list_user =response.data;
    //         $scope.notify(response.data.Info,response.data.Success,"main.list_user")
    //     })
    // }

    $scope.openModalEdit = function (data) {
        console.log("data ini"+data);
          $scope.air=data;
          // alert($scope.detail[0].area.kd_area);
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/airport/modalEdit.html',
            controller: function ($scope, $http,$uibModalInstance) {
              $scope.ok = function () {
                $uibModalInstance.close();
              };

              $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })
      };


      $scope.openModalTambah = function () {
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/admin/user/modalTambah.html',
            controller: function ($scope, $http,$uibModalInstance) {
              $scope.ok = function (data) {
                console.log("iniiiii : "+angular.toJson(data));
                data.creator=$scope.user.nama;
                $scope.saveUser(data);
                $scope.list_user[$scope.list_user.length]=data;
                $scope.us={};
                $uibModalInstance.close();
              };

              $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          })
      };
  $scope.dataTableOpt = {
   //custom datatable options
  // or load data through ajax call also
  "aLengthMenu": [[10, 50, 100,-1], [10, 50, 100,'All']],
  };


      $scope.setValuePreview=function(param) {
        var detail={
          "office_company": param.office_company,
          "area": param.area,
          "registered_addres": param.registered_addres,
          "no_telp": param.no_telp,
          "npwp": param.npwp,
          "location_operation": param.location_operation,
          "hours_service": param.hours_service,
          "commencement_date": param.commencement_date,
          "organization_chart": param.organization_chart,
          "operation_manual": param.operation_manual,
          "facilities_installed": param.facilities_installed,
          "staff_engaged": param.staff_engaged ,
          "service_demonstrated": param.service_demonstrated,
          "organization_hold_casr172": param.organization_hold_casr172,
          "reference": param.reference,
          "detail_reason": param.detail_reason,
          "on_behalf_of": param.on_behalf_of,
          "signed": param.signed,
          "authority": param.authority,
          "name_of_person": param.name_of_person,
          "email": param.email,
          "username": param.username,
          "password": param.password,
          "company_name": param.company_name,
          "ats_aprovided1": (param.ats_aprovided1==1),
          "ats_aprovided2": (param.ats_aprovided2==1),
          "ats_aprovided3": (param.ats_aprovided3==1),
          "ats_aprovided4": (param.ats_aprovided4==1),
          "ats_aprovided5": (param.ats_aprovided5==1),
          "file_no": param.file_no,
          "hours_service1": param.hours_service1,
          "status_register": param.status_register,
          "hours_service2": param.hours_service2,
          "hours_service3": param.hours_service3,
          "hours_service4": param.hours_service4,
          "airport_id": param.airport_id,
          "postal_addres": param.postal_addres,
          "created": param.created
        }
        return detail;
      }
      $scope.previewDetail = function (data) {
        console.log(angular.toJson(data));
        $scope.detail=$scope.setValuePreview(data);

        $uibModal.open({
          animation: true,
          scope: $scope,
          backdrop: 'static',
          templateUrl: 'partials/admin/user/ModalPreview.html',
          controller: function ($scope,$uibModalInstance) {
            $scope.ok = function (data) {
              $uibModalInstance.dismiss('cancel');
            };

            $scope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
            };
          }
        })
      }


$scope.aaproved=function(data){
  console.log("data "+angular.toJson(data));
}


    });
