'use strict';

/* Controllers */

angular.module('AngStarter')
  .controller('AfisCtrl',
    function (
      $scope,
      AfisFactory,
       $cookies,
       $uibModal,
        AdminFactory,
      Pertanyaanfactory) {

      var ConfigApi, dataLogin, id_role;
      $scope.list_p=[];
      $scope.urlGet = "http://localhost:8090/api/sertifikasi/preview/";
      $scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(AFIS)";
      $scope.header="";
      $scope.names=[{'id':0,'val':'Reject'},{'id':1,'val':'Approve'}];

      $scope.AfisDetail=[];
      $scope.step=[];
      $scope.detail = [];
      $scope.selectNow={"kode_user":"","airport_id":""}
      $scope.selectedStatusParent="Tidak Di Terapkan"; 
      $scope.fileParent = null;
      $scope.remaksParent = "";
      $scope.fileChild = null;
      $scope.remaksChild = "";
      $scope.status_detail_pq="";
      $scope.headerChild="";
      $scope.remaksAdmin = "";
      $scope.remaksAdmin2 = "";
      var ConfigApi = {
        headers : {
            "Content-Type": "application/json",
            "token":JSON.parse($cookies.get('dataLogin')).Response.id_token,
            "keyInit":JSON.parse($cookies.get('dataLogin')).Response.user.kode_user,
            "email": ""
        }
      }

      $scope.page = 1;
             $scope.displayItems = $scope.AfisDetail.slice(0, 3);
             $scope.pageChanged = function() {
                var startPos = ($scope.page - 1) * 3;
            };

      $scope.SubmitProg=function(data,status){
        console.log(angular.toJson(data));
        var param ={
          "kode_area":data[0].kode_area,
          "status":status,
          "kode_user":data[0].kode_user,
          "status_lama": 1
        };
        console.log(param);
        AdminFactory.SubmitProg(param,ConfigApi).then(function(res){
          if(res.data.Success){
            $scope.getByInit();
          }
          $scope.notif(res.data.Info,res.data.Success);
        })
      }

    $scope.getBy=function(d,s){
      var data={'type':d,'status':s};
      switch(s){
        case 1:  $scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(AFIS)  / (LIST) "; break;
        case 11: $scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(AFIS)  / (APPROVED) "; break;
        case -1:$scope.title="PROTOCOL QUESTION SERTIFIKASI PENYELENGGARA PELAYANAN LALU LINTAS PENERBANGAN -(AFIS)  / (REJECTED) "; break;
      }
      AfisFactory.getByType(data,ConfigApi).then(function (response) {
            console.log("data user 22"+angular.toJson(response.data.Response));
            if(response.data.Response.length>0){
              $scope.list_p =response.data.Response;
            }else{
              $scope.list_p=[];
              $scope.AfisDetail=[];
            }
        })
    }


    $scope.getByInit=function(){
      $scope.getBy('AFIS',1);
    }
    $scope.getByInit();

    $scope.showDetail = function (data) {
      ConfigApi.headers.email = data.user.kode_user;
       $scope.header='Data dari Airport '+data.user.airport.airport_name+' - Kota : '+data.user.airport.city+' - Kepulauan : '+data.user.airport.island+' - Oleh : '+data.user.nama;
        AfisFactory.getDetailBy(data.kode_prog,data.user.airport.airport_id,ConfigApi).then(function(res){
          $scope.AfisDetail=res.data.Response;
          console.log("data ini" + angular.toJson($scope.AfisDetail));
          $scope.getStep(data.user.kode_user);

        });

      }

      $scope.openDetailBy=function(data){ 
        var d={'pq_number':data.pq.pq_number,'kode_user':data.kode_user};
        AfisFactory.getListDetailBy2(d,ConfigApi).then(function(res){
          if(res.data.Response.length>0){
            $scope.AfisDetailBy=res.data.Response;
            $uibModal.open({
                animation: true,
                templateUrl: 'partials/admin/AFIS/AfisDetail.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                controller: function ($uibModalInstance, $scope) {
                    $scope.closeModalDocDetail = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                size: 'lg'
            });
          }else{
            $scope.notif("tidka ada Detail",false);
          }
        });
      }

      $scope.notif=function(data,status){
        Pace.start()
        if(status){
              $.smallBox({
                  title: "Success!",
                  content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                  color: "#659265",
                  iconSmall: "fa fa-check fa-2x fadeInRight animated",
                  timeout: 4000
              });
        }else{
          $.smallBox({
              title: "Warning!",
              content: "<i>" + data + ".</i>",
              color: "#a65858",
              iconSmall: "fa fa-times fa-2x fadeInRight animated",
              timeout: 4000
          });
        }
          Pace.stop();
      }


      $scope.OpenAction=function(data){
        console.log("data action : "+angular.toJson(data));
        $scope.data1={'kode_user':data.kode_user  ,'pq_number':data.pq.pq_number,'remaks':data.remaks_admin,'status':data.status_admin};
        if(data.status_admin==1){
          $scope.status_approved="Approved";
        }else{
          $scope.status_approved="Rejected";
        }

        $uibModal.open({
            animation: true,
            templateUrl: 'partials/admin/AFIS/ApprovOrReject.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            controller: function ($uibModalInstance, $scope) {
                $scope.closeModalDocDetail = function (status,p2) {


                    if(status==1){
                      $scope.data1.status=1;
                      $scope.ApprovOrreject($scope.data1);
                    }else if(status==2){
                      $scope.data1.status=0;
                      $scope.ApprovOrreject($scope.data1);
                    }
                    let index =   $scope.AfisDetail.findIndex( record => record.pq.pq_number=== data.pq.pq_number )
                      $scope.AfisDetail[index].status_admin=$scope.data1.status;
                        $scope.AfisDetail[index].remaks_admin=$scope.data1.remaks;


                    console.log("data modal : "+angular.toJson(p2)+" index"+index);
                    $uibModalInstance.dismiss('close');
                };

            },
            size: 'lg'
        });
      }

      $scope.ApprovOrreject=function(remaks,status,pq_number){
       var data = {
              "airport_id": $scope.selectNow.airport_id,
                "kode_area": "AFIS",
                "kode_user": $scope.selectNow.kode_user,
                "pq_number": pq_number,
                "remaks": remaks,
                "status": status,
                "status_lama": 1
              }
        AdminFactory.ApprovOrReject(data, ConfigApi).then(function(res){
          console.log("data " + angular.toJson(res));
        });
      }



      $scope.detailAfis = function (d) {
        if (d) {
          $uibModal.open({
            animation: true,
            templateUrl: 'partials/admin/AFIS/AfisPoint2.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            controller: function ($uibModalInstance, $scope) {
              $scope.closeModalDocDetail = function () {
                $uibModalInstance.dismiss('cancel');
              }
            },
            size: 'lg'
          });
        }
      }

      $scope.switchStep = function (data, step) {
        let index = (parseInt(step) - 1);
        console.log(index + " data " + data);

        if (data) {
          $scope.steps[index].step = step;
          if(step==2){
            $scope.detailAfis(true);
          }
        } else {
          $scope.steps[index].step = 0;
        }
        $scope.saveStep($scope.steps[index]);
      }

      $scope.getStep = function (data) {
        ConfigApi.headers.email = data;
        AfisFactory.getStep(ConfigApi).then(function (res) {
          if (res.data.Success) {
            $scope.steps = res.data.Response;
          }
          for (let i = 0; i < 11; i++) {
            if ($scope.steps[i].step != 0) {
              $scope.steps[i].checked = true;
            }
          }
          console.log("step : " + angular.toJson($scope.steps));
        });
      }
    

      $scope.saveStep = function (data) {
        AfisFactory.putStep(data, ConfigApi).then(function(res) {
          if (res.data.Success) {
          }
        });
      }



      /* ================================================= */
      $scope.netralParent = function () {
      $scope.headerParent="";
        $scope.selectedStatusParent="Belum DI Terapkan";
        $scope.fileParent = null;
        $scope.remaksParent = "";
        $scope.selectNow.kode_user = null;
        $scope.selectNow.airport_id = null;
        $scope.remaksAdmin = "";
      }
      $scope.netralChild = function () {
        $scope.headerChild = "";
        $scope.remaksChild = "";
        $scope.fileChild = null;
        $scope.status_detail_pq="Belum Di Terapkan";
        $scope.remaksAdmin2 = "";

      }

      $scope.setValueChild = function (data) {
        console.log(data);
        $scope.headerChild = "Pengajuan Detail Protocol Question " + data.Response.kode_detail_pq;
        $scope.remaksChild = data.Response.remarks;
        $scope.remaksAdmin2 = data.Response.remaks_admin;

        $scope.fileChild = data.file;
        
        switch (data.Response.status_detail_pq) {
          case 1:
            $scope.status_detail_pq = "Sesuai";
            break;

          case 2: $scope.status_detail_pq = "Tidak Sesuai";

            break;
          case 3: $scope.status_detail_pq = "Belum Di Terapkan";

            break;
        }
      }

      $scope.setValueParent = function (data, file) {
        $scope.remaksAdmin = data.remaks_admin;
        $scope.selectNow.kode_user=data.kode_user;
        $scope.selectNow.airport_id=data.airport_id;
        $scope.remaksParent = data.remarks;
        $scope.fileParent = file;
        switch (data.status_pq) {
          case 1:
            $scope.selectedStatusParent = "Sesuai";
            break;

          case 2: $scope.selectedStatusParent = "Tidak Sesuai";

            break;
          case 3: $scope.selectedStatusParent = "Belum Di terapkan";

            break;
        }
      }

      $scope.open = function (pq_number) {
        $uibModal.open({
          animation: true,
          scope: $scope,
          backdrop: 'static',
          templateUrl: 'partials/admin/AFIS/QuestionDetail.html',
          controller: function ($scope,$uibModalInstance) {
            $scope.ok = function (remaks,status) {
              $scope.ApprovOrreject(remaks,status,pq_number);
              $scope.netralParent();
              $uibModalInstance.dismiss('cancel');
            };
            $scope.cancel = function () {
              $scope.netralParent();
              $uibModalInstance.dismiss('cancel');
            };
          },
          size: 'lg'
        })
      }


      $scope.openModal = function (data) {
        $scope.headerParent = "Pengajuan Protocol Question " + data.pq.pq_number;
        ConfigApi.headers.email=data.kode_user;
        Pertanyaanfactory.getDetailByAdmin(data.pq.pq_number, data.kode_area, ConfigApi).then(function (res) {
          var d = res.data;
          $scope.detail = d.Response;
          if (d.jawaban != null) {
            $scope.setValueParent(d.jawaban, d.file);
          }
          console.log("detailll "+angular.toJson(data));
          $scope.open(data.pq.pq_number);
        });

      }
//ini benerin
      

      $scope.updateChild=function(data){
        console.log("data sebelum nya (request) : "+angular.toJson(data));
        AdminFactory.ApprovOrRejectDetail(data,ConfigApi).then(function (res) {
          console.log(res);
        });
      }

      $scope.viewDetailInDetail = function(data) {
       // console.log(angular.toJson(data));
        var req = {
          "airport_id": $scope.selectNow.airport_id,
          "kode_area": data.area.kd_area,
          "kode_kategory_pq": data.kategory.kode_kategory_pq,
          "kode_detail_pq": data.kode_detail_pq,
          "kode_user": $scope.selectNow.kode_user,
          "pq_number": data.pq_number,
          "remaks_admin":"",
          "status_detail_admin":""
        };
        

        AfisFactory.questionDetailBy(req, ConfigApi).then(function (res) {
          $scope.setValueChild(res.data);
          //console.log("data child "+angular.toJson(res));
        });
          
          $uibModal.open({
            animation: true,
            templateUrl: "partials/admin/AFIS/QuestionInDetail.html",
            scope: $scope,
            backdrop: "static",
            keyboard: false,
            controller: function($uibModalInstance, $scope) {
              $scope.approveOrReject = function(remaks,status) {
                req.remaks_admin=remaks;
                req.status_detail_admin=status;
                req.kode_kategory= data.kategory.kode_kategory_pq,
                $scope.updateChild(req);
                $scope.netralChild();
                $uibModalInstance.dismiss("cancel");
              },
              $scope.close=function(){
                $scope.netralChild();
                $uibModalInstance.dismiss("cancel");
              }
          },
            size: "lg"
          });
        
      };

    });
