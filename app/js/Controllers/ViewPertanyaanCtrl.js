"use strict";
angular.module("AngStarter").
controller('ViewPertanyaanCtrl',function(
  $scope,
  $http,
  $uibModal,
  $cookies,
  $state,
  SweetAlert,
  RegisterFactory,
AfisFactory){

$scope.list_p=[];
$scope.detail=[];
$scope.status={};
$scope.page = 1;


// if($scope.users.length<1){
//   alert("paras"+$scope.users.length);
//
// }
var datl=JSON.parse($cookies.get('dataLogin')).Response;
 var ConfigApi = {
            headers : {
                "Content-Type": "application/json",
                "token":datl.id_token,
                "keyInit":datl.user.kode_user
            }
        }
        var ConfigApiUp = {
          headers : {
              "Content-Type": undefined,
              "token":datl.id_token,
                "keyInit":datl.user.kode_user
          }
      }

 $scope.History= function() {
   var datl=JSON.parse($cookies.get('dataLogin')).Response;
   var d=datl.user.kode_user;
  AfisFactory.getDetailBy(d,ConfigApi).then(function(data) {
            $scope.list_p = data.data['Response'];
          });
          console.log($scope.dataPost);
  }

  $scope.displayItems = $scope.list_p.slice(0, 5);
  $scope.pageChanged = function() {
     var startPos = ($scope.page - 1) * 5;
     };


     $scope.openSPDA_Modal = function (data) {
         $scope.headerModal = {
           noProtocol: data.pq.pq_number,
         }
       AfisFactory.getDetailDocBy(data.id_pq,ConfigApi).then(function(res){
           if(res.data.Response.length>0){
               $scope.AfisDetailDoc=res.data.Response;
               $uibModal.open({
                   animation: true,
                   templateUrl: 'partials/admin/AFIS/formUpload.html',
                   scope: $scope,
                   backdrop: 'static',
                   keyboard: false,
                   controller: function ($uibModalInstance, $scope) {
                       $scope.closeModalDocDetail = function () {
                           $uibModalInstance.dismiss('cancel');
                       };
                   },
                   size: 'md'
               });
            }else{
               $scope.notif("Tidak Ada Lampiran");
            }
     });
     }


     $scope.notif=function(data,status){
       Pace.start()
       if(status){
             $.smallBox({
                 title: "Success!",
                 content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                 color: "#659265",
                 iconSmall: "fa fa-check fa-2x fadeInRight animated",
                 timeout: 4000
             });
       }else{
         $.smallBox({
             title: "Warning!",
             content: "<i>" + data + ".</i>",
             color: "#a65858",
             iconSmall: "fa fa-times fa-2x fadeInRight animated",
             timeout: 4000
         });
       }
         Pace.stop();
     }


     $scope.openDetailBy=function(data){
       var d={'pq_number':data.pq.pq_number,'kode_user':data.kode_user};
       var pilihan = [
         '-',
         'Tidak Diterapkan',
         'Sesuai',
         'Tidak Sesuai'
       ];
         $scope.header=data.pq.pq_number;
       AfisFactory.getListDetailBy2(d,ConfigApi).then(function(res){
         if(res.data.Response.length>0){
           $scope.AfisDetailBy=res.data.Response;
         //  console.log("detail "+angular.toJson($scope.AfisDetailBy));
           $uibModal.open({
               animation: true,
               templateUrl: 'partials/admin/ATC/AtcDetail.html',
               scope: $scope,
               backdrop: 'static',
               keyboard: false,
               controller: function ($uibModalInstance, $scope) {
                   $scope.closeModalDocDetail = function () {
                       $uibModalInstance.dismiss('cancel');
                   },

                   $scope.translate=function(s){
                     return  pilihan[s];
                   };
               },
               size: 'lg'
           });
         }else{
           $scope.notif("tidka ada Detail",false);
         }
       });
     }



     $scope.openSPDA_ModalDetail = function (kode,airport) {
         $scope.headerModal = {
           noProtocol: kode+"  - Airport : "+airport,
         }
       AfisFactory.getDetailDocByDetail(kode,airport,ConfigApi).then(function(res){
           if(res.data.Response.length>0){
               $scope.AfisDetailDoc=res.data.Response;
               $uibModal.open({
                   animation: true,
                   templateUrl: 'partials/admin/ATC/formUpload.html',
                   scope: $scope,
                   backdrop: 'static',
                   keyboard: false,
                   controller: function ($uibModalInstance, $scope) {
                       $scope.closeModalDocDetail = function () {
                           $uibModalInstance.dismiss('cancel');
                       };
                   },
                   size: 'md'
               });
            }else{
               $scope.notif("Tidak Ada Lampiran");
            }
     });
     }
});
