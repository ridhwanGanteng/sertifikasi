"use strict";
angular.module("AngStarter").
controller('CapCtrl',function(
  $scope,
  $uibModal,
  Pertanyaanfactory
  ,$cookies,

  CapFactory){
  $scope.users = []; 
  $scope.detail = { "compelation_date": null, "action_office": "", "proposed_action": "","evidance_reference":""};
$scope.user=null;
$scope.use=null;
$scope.names = [{"id":1,"value":"Sesuai"},{"id":2,"value":"Tidak Sesuai"},{"id":3,"value":"Belum Ditentukan"}];
$scope.file=[];
$scope.fileParent = [];
$scope.parent=[];
$scope.child=[];
$scope.page = 1;
$scope.urlGet = "http://localhost:8090/api/sertifikasi/preview/";
       $scope.displayItems = $scope.users.slice(0, 3);
       $scope.pageChanged = function() {
          var startPos = ($scope.page - 1) * 3;


        };

// if($scope.users.length<1){
//   alert("paras"+$scope.users.length);
//
// }

var datl=JSON.parse($cookies.get('dataLogin')).Response;
 var ConfigApi = {
            headers : {
                "Content-Type": "application/json",
                "token":datl.id_token,
                "keyInit":datl.user.kode_user,
                "email": datl.user.kode_user

            }
        }
        var ConfigApiUp = {
          headers : {
              "Content-Type": undefined,
              "token":datl.id_token,
                "keyInit":datl.user.kode_user
          }
      }


$scope.capInit=function(){
  var d={
    "airport_id":datl.user.airport.airport_id,
    "kode_area":datl.user.area
    };
console.log(d);
  CapFactory.IsTrueReject(d,ConfigApi).then(function(res2){
      if(res2.data.Response==1){
        CapFactory.getBy(d,ConfigApi).then(function(res){
          $scope.users=res.data.Response;
          console.log("data : "+angular.toJson($scope.users));
        });
      }
    })

}

$scope.setValue=function(data,parent){
  console.log("ini data parent " + angular.toJson(parent));
  //console.log("ini data parent22 " + angular.toJson(data));
  $scope.parent=parent.Response;
  $scope.parent.file=parent.file;
  if (parent.Response.status_pq==1){
    $scope.parent.status = "Sesuai";
  } else if (parent.Response.status_pq == 2){
    $scope.parent.status = "Tidak Sesuai";
  }else{
    $scope.parent.status = "Belum DiTerapkan"; 
  }

  if (parent.Response.status_admin == 1) {
    $scope.parent.status_admin = "Approved";
  } else if (parent.Response.status_pq == 2) {
    $scope.parent.status_admin = "Rejected";
  } else {
    $scope.parent.status_admin = "Belum DiTerapkan";
  }
  $scope.use.compelation_date = data.compelation_date;
  $scope.use.compelation_date = new Date($scope.use.compelation_date);
  //$scope.detail.compelation_date = data.compelation_date;
  $scope.parent.detail=parent.child;
  $scope.parent.file_upload=parent.capfile;
}


$scope.netralParent=function() {
  $scope.parent=[];
  console.log("data parent netral : "+angular.toJson($scope.parent));
}

$scope.netralChild=function(){
    $scope.detail.airport = data.airport_id;
  $scope.detail.pq_number = data.pq.pq_number;
  $scope.detail.proposed_action = $scope.use.proposed_action;
  $scope.detail.evidance_reference = $scope.use.evidance_reference;
  $scope.detail.kode_area = $scope.use.kode_area;
  $scope.detail.airport_id = $scope.use.airport_id;
  
}

   $scope.openModal = function (data) {
     var d={
       "kode_area":data.kode_area,
       "pq_number":data.pq_number,
       "airport":data.airport_id
     }
    
      CapFactory.getParentReject(d, ConfigApi).then(function (res) {
       // console.log("response "+angular.toJson(res));
        if (res.data.Response!=null){
          console.log("ini parma nya1 :" + angular.toJson(data));
          $scope.setValue(data,res.data);
        }
      });
     
          $scope.use=data;
          // alert($scope.detail[0].area.kd_area);
          
          $uibModal.open({
            animation: true,
            scope:$scope,
            backdrop  : 'static',
            templateUrl: 'partials/pertanyaan/QuestionCap.html',
            controller: function ($scope, $http,$uibModalInstance) {
              $scope.oke = function () {
                
                var datapost = {
                  action_office: $scope.use.action_office,
                  airport: $scope.use.airport_id,
                  compelation_date: $scope.use.compelation_date,
                  evidance: $scope.use.evidance_reference,
                  file_upload: $scope.parent.file_upload,
                  kode_area: $scope.use.kode_area,
                  kode_kategory_pq: $scope.use.kode_kategory,
                  kode_user: $scope.use.kode_user,
                  pq_number: $scope.use.pq_number,
                  proposed: $scope.use.proposed_action
                };
                console.log("data final : " + angular.toJson(datapost));
                CapFactory.PutCap(datapost,ConfigApi).then(function (res) {
                
                //$scope.notif(res.data.Info,res.data.Success);
                
                });
                $scope.netralParent();
                $uibModalInstance.dismiss("cancel");
                
            },
              $scope.close = function () {
                $scope.netralParent();
                $uibModalInstance.dismiss('cancel');
              };
            }
          })
      };


    $scope.SubmitCap=function(){
      var datl=JSON.parse($cookies.get('dataLogin')).Response;
      var d={
        "airport_id":datl.user.airport.airport_id,
        "kode_area":datl.user.area,
        "kode_user":datl.user.kode_user,
        "status":111
        };

        CapFactory.PutCapStatus(d,ConfigApi).then(function(res){
          $scope.notif(res.data.Info,res.data.Success);
        });

    }


      $scope.notif=function(data,status){
        Pace.start()
        if(status){
              $.smallBox({
                  title: "Success!",
                  content: "<i class='fa fa-checklist'></i> <i>" + data + ".</i>",
                  color: "#659265",
                  iconSmall: "fa fa-check fa-2x fadeInRight animated",
                  timeout: 4000
              });
        }else{
          $.smallBox({
              title: "Warning!",
              content: "<i>" + data + ".</i>",
              color: "#a65858",
              iconSmall: "fa fa-times fa-2x fadeInRight animated",
              timeout: 4000
          });
        }
          Pace.stop();
      }


  $scope.updateDataLampiran = function (file) {
    var fd = new FormData();
    //Take the first selected file
    for (var i = 0; i < file.length; i++) {
      fd.append("fileUpload", file[i]);
    }
    Pertanyaanfactory.upload(fd, ConfigApiUp).then(function (res) {
      var data = res.data.Response;
      console.log(data);
      $scope.parent.file_upload = angular
        .copy(data || [])
        .map(function(val) {
          return {
            idx: val.idx,
            link_lampiran: val.link_lampiran,
            path_file: val.path_file
          };
        });
      console.log($scope.fileParent);
    })

  }

  $scope.updateDataLampiranChild = function (file) {
    var fd = new FormData();
    //Take the first selected file
    for (var i = 0; i < file.length; i++) {
      fd.append("fileUpload", file[i]);
    }
    Pertanyaanfactory.upload(fd, ConfigApiUp).then(function (res) {
      var data = res.data.Response;
      $scope.child.cap_file = angular
        .copy(data || [])
        .map(function (val) {
          return {
            idx: val.idx,
            link_lampiran: val.link_lampiran,
            path_file: val.path_file
          };
        });
    })

  }
      $scope.cekRow=function(user){
      if (user.compelation_date == null 
          || user.action_office == null 
          || user.proposed_action == null 
          || user.evidance_reference == null){
          return "red";
    }
        return "powderblue";
  }


  $scope.setValueChild=function(data){
    var karena=data.karena;
    var cap=data.Response;
    if(karena.status_detail_pq==1){
      $scope.child.before_status = "Sesuai";
    } else if (karena.status_detail_pq == 2){
      $scope.child.before_status = "Tidak Sesuai";
    }else{
      $scope.child.before_status = "Belum Di Terapkan";
    }
    $scope.child.before_remarks = karena.remarks;
    $scope.child.before_remarks_admin = karena.remaks_admin;
    $scope.child.before_file=data.file;

    $scope.child.cap_compelation_date = cap.compelation_date;
    $scope.child.cap_compelation_date = new Date($scope.child.cap_compelation_date);
    $scope.child.cap_action_office=cap.action_office;
    $scope.child.cap_proposed_action = cap.proposed_action;
    $scope.child.cap_evidance_reference = cap.evidance_reference;
    $scope.child.cap_airport = cap.airport_id;
    $scope.child.cap_kategory=cap.kode_kategory_pq;
    $scope.child.cap_pq_number=cap.pq_number;
    $scope.child.cap_kode_detail=cap.kode_detail_pq;
    $scope.child.cap_user=karena.kode_user;
    $scope.child.cap_file=data.capfile;
    $scope.child.cap_area=cap.kode_area;
    
  }

  $scope.postChild=function(){
    /*  */
    var post={
      "action_office": $scope.child.cap_action_office,
        "airport": $scope.child.cap_airport,
        "compelation_date": $scope.child.cap_compelation_date,
        "evidance": $scope.child.cap_evidance_reference,
        "file_upload": $scope.child.cap_file,
        "kode_area": $scope.child.cap_area,
        "kode_detail_pq": $scope.child.cap_kode_detail,
      "kode_kategory_pq": $scope.child.cap_kategory,
        "kode_user": $scope.child.cap_user,
        "pq_number": $scope.child.cap_pq_number,
        "proposed": $scope.child.cap_proposed_action
    }
    CapFactory.PutCapDetail(post, ConfigApi).then(function(res) {
      $scope.notif(res.data.Info, res.data.Success);
    });
    console.log(angular.toJson(post));
  }

  $scope.viewDetailInDetail=function(data){
    var param={
      "kode_area":data.kode_area,
      "airport_id":data.airport_id,
      "pq_number": data.pq_number,
      "kode_detail_pq":data.detail_pq.kode_detail_pq
    }
    console.log("data dari parent : "+angular.toJson(data));
    CapFactory.getCapDetail(param,ConfigApi).then(function(res){
      if(res.data.Response!=null){
        $scope.setValueChild(res.data);
      }
    });
    $uibModal.open({
      animation: true,
      scope: $scope,
      backdrop: 'static',
      templateUrl: 'partials/pertanyaan/CapdetailInDetail.html',
      controller: function ($scope, $http, $uibModalInstance) {
        $scope.oke = function () {
          $scope.postChild();
          
          
          //$scope.netralParent();
          $uibModalInstance.dismiss("cancel");

        },
          $scope.close = function () {
            //$scope.netralParent();
            $uibModalInstance.dismiss('cancel');
          };
      }
    })
  }
});

