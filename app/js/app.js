"use strict";
// Declaramos la aplicacion y definimos sus dependencias
angular.module('AngStarter', 
[
'ngRoute',
'ui.bootstrap',
'ngStorage',
'ui.router',
'ui.router.state.events',
'ui.load',
'oc.lazyLoad',
'ngAnimate',
'ngCookies',
// 'ui.jq',
'ngResource',
'ngSanitize',
'ngTouch',
'ui.utils',
'ui.bootstrap.datetimepicker',
'daterangepicker',
'ui.utils.masks',
'chart.js',
'tc.chartjs',
'ngAria',
'ngMaterial',
'ngMaterialDatePicker',
'ui.utils',
'angular-steps'
]);