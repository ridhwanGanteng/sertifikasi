'use strict';
var url = "";
angular.module('AngStarter')
    /** login, logout, register */
    .factory('loginFactory', function ($http, Config) {
        return {
            login: function (data, headers) {
                url = Config.api + '/login';
                // console.log(headers);
                return $http.post(url, data, headers);
            },
            logout: function (data, headers) {
                url = Config.api + '/api/logout';
                return $http.post(url, data, headers);
            },
            register: function (data, headers) {
                url = Config.api + '/api/registrasi';
                return $http.post(url, data, headers);
            }
        };
    })
    .factory('Pertanyaanfactory', function ($http, Config) {
        return {
            getPertanyaan: function (data, headers) {
                url = Config.api + "/pertanyaan/" + data;
                // console.log(headers);
                return $http.get(url, headers);
            }, save: function (data, headers) {
                url = Config.api + "/tpq";
                return $http.post(url, data, headers);
            }, upload: function (data, headers) {
                url = Config.api + "/tpq_upload";
                return $http.post(url, data, headers);
            },
            getDetailByAdmin: function (data, d, headers) {
                url = Config.api + "/pertanyaan_detail/bypqAdmin/" + data + "/" + d;
                return $http.get(url, headers);
            },
            getDetailBy: function (data, d, headers) {
                url = Config.api + "/pertanyaan_detail/bypq/" + data + "/" + d;
                return $http.get(url, headers);
            }, submitToadmin: function (data, headers) {
                url = Config.api + "/submitToadmin";
                return $http.post(url, data, headers);
            }
        };
    })

    .factory('NavFactory', function ($http, Config) {
        return {
            getMenu: function (data, headers) {
                url = Config.api + '/menucollection/' + data;
                // console.log(headers);
                return $http.get(url, headers);
            }
        };
    })

    .factory('AirportFactory', function ($http, Config) {
        return {
            getAllAirport: function (headers) {
                url = Config.api + '/airpot';
                return $http.get(url, headers);
            },
            save: function (data, headers) {
                url = Config.api + '/airpot';
                return $http.post(url, data, headers);
            },
            update: function (data, headers) {
                url = Config.api + '/airpot';
                return $http.put(url, data, headers);
            },
            delete: function (data, headers) {
                url = Config.api + '/airpot/' + data;
                return $http.delete(url, headers);
            },
            getAllAirportClient: function (headers) {
                url = Config.api + '/airpot_client';
                return $http.get(url, headers);
            }

        };
    })

    .factory('UserFactory', function ($http, Config) {
        return {
            getAllUser: function (headers) {
                url = Config.api + '/userAll';
                return $http.get(url, headers);
            },

            getReporting: function (headers) {
                url = Config.api + '/getReporting';
                return $http.get(url, headers);
            },
            getAllUserBy: function (data, headers) {
                url = Config.api + '/userAllBy' + data;
                return $http.get(url, headers);
            },
            save: function (data, headers) {
                url = Config.api + '/user';
                return $http.post(url, data, headers);
            },
            update: function (data, headers) {
                url = Config.api + '/user';
                return $http.put(url, data, headers);
            },
            delete: function (data, headers) {
                url = Config.api + '/airpot/' + data;
                return $http.delete(url, headers);
            }
        };
    })
    .factory('RegisterFactory', function ($http, Config) {
        return {
            getAllUser: function (headers) {
                url = Config.api + '/register';
                return $http.get(url, headers);
            },
            getByType: function (data, headers) {
                url = Config.api + '/register/' + data;
                return $http.get(url, headers);
            },
            getUserAll: function (headers) {
                url = Config.api + '/registerALl';
                return $http.get(url, headers);
            },
            saveApproved: function (data, headers) {
                url = Config.api + '/registerApproved';
                return $http.post(url, data, headers);
            },
            save: function (data, headers) {
                url = Config.api + '/register';
                return $http.post(url, data, headers);
            },
            update: function (data, headers) {
                url = Config.api + '/register';
                return $http.put(url, data, headers);
            },
            delete: function (data, headers) {
                url = Config.api + '/register/' + data;
                return $http.delete(url, headers);
            }
        };
    })

    .factory('AfisFactory', function ($http, Config) {
        return {

            getStatusPertanyaan: function (data, headers) {
                url = Config.api + "/getStatusPertanyaan/" + data.type;
                return $http.get(url, headers);
            }
            ,
            getAll: function (headers) {
                url = Config.api + '/list_pengajuan';
                return $http.get(url, headers);
            },
            getByType: function (data, headers) {
                url = Config.api + '/list_pengajuanBy/' + data.type + '/' + data.status;
                return $http.get(url, headers);
            },
            getDetailBy: function (data, dd, headers) {
                url = Config.api + '/list_pengajuanDetailBy/' + data + '/' + dd;
                return $http.get(url, headers);
            },
            getDetailDocBy: function (data, headers) {
                url = Config.api + '/list_pengajuanDetailDocByIdPq/' + data;
                return $http.get(url, headers);
            },
            getDetailDocByDetail: function (data, airport, headers) {
                url = Config.api + '/list_pengajuanDetailDocByIdPq2/' + data + '/' + airport;
                return $http.get(url, headers);
            },
            getListDetailBy2: function (data, headers) {
                url = Config.api + '/list_Detail_question/' + data.pq_number + '/' + data.kode_user;
                return $http.get(url, headers);
            },
            check: function (data, headers) {
                url = Config.api + '/checkStatus2/' + data.type + '/' + data.user;
                return $http.get(url, headers);
            },
            getStatus: function (data, headers) {
                url = Config.api + '/getStatus/' + data.type + '/' + data.status;
                return $http.get(url, headers);
            },
            isUserWrite: function (data, headers) {
                url = Config.api + '/isUserWrite/' + data.type;
                return $http.get(url, headers);
            },
            questionDetailBy: function (data, headers) {
                url = Config.api + '/questionDetailBy';
                return $http.post(url, data, headers);
            },

            getStep: function (headers) {
                url = Config.api + '/step';
                return $http.get(url, headers);
            },
            saveStep: function (data, headers) {
                url = Config.api + '/step';
                return $http.post(url, data, headers);
            },
            delStep: function (data, headers) {
                url = Config.api + '/delstep';
                return $http.post(url, data, headers);
            },
            putStep: function (data, headers) {
                url = Config.api + '/step';
                return $http.put(url, data, headers);
            },
            saveSingle: function (data, headers) {
                url = Config.api + '/saveSingle';
                return $http.post(url, data, headers);
            }

        };
    })

    .factory('AdminFactory', function ($http, Config) {
        return {
            ApprovOrReject: function (data, headers) {
                url = Config.api + '/admin/ApproveOrReject';
                return $http.put(url, data, headers);
            },
            ApprovOrRejectDetail: function (data, headers) {
                url = Config.api + '/admin/ApproveOrRejectDetail';
                return $http.put(url, data, headers);
            },

            SubmitProg: function (data, headers) {
                url = Config.api + '/admin/ApproveOrRejectProggres';
                return $http.post(url, data, headers);
            }
        };
    })
    .factory('MasterFactory', function ($http, Config) {
        return {
            getBy: function (data, headers) {
                url = Config.api + '/pertanyaan/' + data.type;
                return $http.get(url, headers);
            },
            save: function (data, headers) {
                url = Config.api + '/pertanyaan';
                return $http.post(url, data, headers);
            },
            put: function (data, headers) {
                url = Config.api + '/pertanyaan';
                return $http.put(url, data, headers);
            },
            getKategory: function (headers) {
                url = Config.api + '/kategory/';
                return $http.get(url, headers);
            },
            getDetailBy: function (d1, d2, headers) {
                url = Config.api + '/pertanyaan_detail/bypq/' + d1 + '/' + d2;
                return $http.get(url, headers);
            },

        };
    })
    .factory('PdfFactory', function ($http, Config) {
        return {
            preview: function (data, headers) {
                url = Config.api + '/preview/' + data;
                return $http.get(url, headers);
            }

        };
    })
    .factory('CapFactory', function ($http, Config) {

        return {
            getParentReject: function (data, headers) {
                url = Config.api + "/cap/getParentReject/" + data.kode_area + "/" + data.airport + "/" + data.pq_number;
                return $http.get(url, headers);
            },
            getBy: function (data, headers) {
                url = Config.api + '/cap/listRejected/' + data.kode_area + '/' + data.airport_id;
                return $http.get(url, headers);
            },

            getCapDetail: function (data, headers) {
                url = Config.api + "/cap/getCapDetail/" + data.kode_area + "/" + data.airport_id + "/" + data.pq_number + "/" + data.kode_detail_pq;
                return $http.get(url, headers);
            },
            IsTrue: function (data, headers) {
                //{type}/{status}/{kode_user}
                url = Config.api + '/checkStatus/' + data.kode_area + '/' + 11;
                return $http.get(url, headers);
            },
            IsTrueReject: function (data, headers) {
                //{type}/{status}/{kode_user}
                url = Config.api + '/checkStatus/' + data.kode_area + '/' + -11;
                return $http.get(url, headers);
            },
            PutCap: function (data, headers) {
                url = Config.api + '/cap/correct/';
                return $http.put(url, data, headers);
            },
            PutCapDetail: function (data, headers) {
                url = Config.api + '/cap/correctDetail/';
                return $http.put(url, data, headers);
            },
            PutCapStatus: function (data, headers) {
                url = Config.api + '/cap/submited';
                return $http.put(url, data, headers);
            },
            getCap: function (data, headers) {
                url = Config.api + '/cap/listRejected/' + data.kode_area + '/' + data.airport_id;
                return $http.get(url, headers);
            }


        };
    })
    .factory('PekerjaanFactory', function ($http, Config) {
        return {
            getAll: function(headers) {
                url = `${Config.api}/listPengajuanMaster`;
                return $http.get(url, headers);
            },
            getPic: function(data, headers) {
                url = `${Config.api}/userAllBy${data}`;
                return $http.get(url, headers);
            },
            save: function(data, headers) {
                url = `${Config.api}/saveJobForPic`;
                return $http.post(url, data, headers);
            }
        }
    })