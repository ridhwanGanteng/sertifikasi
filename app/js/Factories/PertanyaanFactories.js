'use strict';
var url="";
angular.module('AngStarter')
    .factory('PertanyaanFactory', function ($http, Config) {
        return {
            login: function (data, headers) {
                url = Config.api + 'csr/public/api/login';
                return $http.post(url, data, headers);
            },
            logout: function (data, headers) {
                url = Config.api + '/api/logout';
                return $http.post(url, data, headers);
            },
            register: function (data, headers) {
                url = Config.api + '/api/registrasi';
                return $http.post(url, data, headers);
            }
        };
    })